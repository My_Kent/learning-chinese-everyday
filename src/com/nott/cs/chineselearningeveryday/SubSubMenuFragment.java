package com.nott.cs.chineselearningeveryday;

import java.util.ArrayList;
import java.util.List;

import com.nott.cs.chineselearningeveryday.model.SubSubMenuClass;
import com.nott.cs.chineselearningeveryday.util.DataSource;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

public class SubSubMenuFragment extends Fragment {
	private DataSource datasource;
	FragmentListener mCallback;

	public static final String PARENT_TYPE_ID="parent_type_id";
	int parent_type;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.phrase_book_page, container, false);
		datasource = new DataSource(getActivity());
		datasource.open();
		
		List<SubSubMenuClass> subSubMenuList = new ArrayList<SubSubMenuClass>();
		
		parent_type = getArguments().getInt(PARENT_TYPE_ID);
		
		Log.w("parentID",parent_type+"");
		
		if (parent_type==2) {
			subSubMenuList=datasource.getAllSubSubMenuProClass();
		} else {
			subSubMenuList=datasource.getAllSubSubMenuWriteClass();
		}
		
		for (int i=0; i<subSubMenuList.size(); i++) {
			LinearLayout ll = (LinearLayout) inflater.inflate(R.layout.sub_sub_menu_item, container, false);
			TextView title = (TextView) ll.findViewById(R.id.title);
			title.setText(subSubMenuList.get(i).getTitle());
			TextView content = (TextView) ll.findViewById(R.id.content_text);
			content.setText(subSubMenuList.get(i).getDescription());
			ll.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1f));
			ll.setId(i+1);
			ll.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					mCallback.showItem(1, parent_type, v.getId());
				}
			});
			((LinearLayout)rootView).addView(ll);
		}
		
		datasource.close();
		//mCallback.showRightMenu(rightMenuID);
		return rootView;
	}

	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mCallback = (FragmentListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement FragmentListener");
		}
	}
}
