package com.nott.cs.chineselearningeveryday.model;

public class IntroClass {
	int ID = 0;
	String img;

	public IntroClass() {

	}

	public IntroClass(int ID, String img) {
		this.ID = ID;
		this.img = img;
	}

	public IntroClass(String img) {
		this.img = img;
	}

	public void setID(int ID) {
		this.ID = ID;
	}

	public int getID() {
		return ID;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getImg() {
		return img;
	}

}
