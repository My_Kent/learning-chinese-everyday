package com.nott.cs.chineselearningeveryday.model;

/**
 * All JUnit tests passed
 * 
 * @author Martin Handley mxh22u
 * @author Vm Tung txv03u
 * 
 */
public class ConversationLineClass {
	int type_id = 0;
	int lineID = 0;
	String img;
	int orderID = 0;
	String speaker;
	String chi;
	String pinyin;
	String eng;
	String audio;

	public ConversationLineClass() {

	}

	public ConversationLineClass(int type_id, String img, int orderID, String speaker, String chi, String pinyin,
			String eng, String audio) {
		this.type_id = type_id;
		this.img = img;
		this.orderID = orderID;
		this.speaker = speaker;
		this.chi = chi;
		this.pinyin = pinyin;
		this.eng = eng;
		this.audio = audio;
	}

	public ConversationLineClass(int type_id, int lineID, String img, int orderID, String speaker, String chi,
			String pinyin, String eng, String audio) {
		this.type_id = type_id;
		this.lineID = lineID;
		this.img = img;
		this.orderID = orderID;
		this.speaker = speaker;
		this.chi = chi;
		this.pinyin = pinyin;
		this.eng = eng;
		this.audio = audio;
	}

	public void setSceneID(int scene_id) {
		this.type_id = scene_id;
	}

	public int getSceneID() {
		return type_id;
	}

	public void setLineID(int lineID) {
		this.lineID = lineID;
	}

	public int getLineID() {
		return lineID;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getImg() {
		return img;
	}

	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}

	public int getOrderID() {
		return orderID;
	}

	public void setSpeaker(String speaker) {
		this.speaker = speaker;
	}

	public String getSpeaker() {
		return speaker;
	}

	public void setChi(String chi) {
		this.chi = chi;
	}

	public String getChi() {
		return chi;
	}

	public void setPinyin(String pinyin) {
		this.pinyin = pinyin;
	}

	public String getPinyin() {
		return pinyin;
	}

	public void setEng(String eng) {
		this.eng = eng;
	}

	public String getEng() {
		return eng;
	}

	public void setAudio(String audio) {
		this.audio = audio;
	}

	public String getAudio() {
		return audio;
	}
}
