package com.nott.cs.chineselearningeveryday.model;

/**
 * All JUnit tests passed
 * 
 * @author Martin Handley mxh22u
 * @author Vm Tung txv03u
 * 
 */
public class LastAccessClass {
	int accessOrder = 0;
	int scenarioTypeID = 0;

	public LastAccessClass() {
	}

	public LastAccessClass(int accessOrder, int scenarioTypeID) {
		this.accessOrder = accessOrder;
		this.scenarioTypeID = scenarioTypeID;
	}

	public void setAccessOrder(int accessOrder) {
		this.accessOrder = accessOrder;
	}

	public int getAccessOrder() {
		return accessOrder;
	}

	public void setScenarioTypeID(int scenarioTypeID) {
		this.scenarioTypeID = scenarioTypeID;
	}

	public int getScenarioTypeID() {
		return scenarioTypeID;
	}
}
