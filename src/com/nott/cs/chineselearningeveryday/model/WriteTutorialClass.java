package com.nott.cs.chineselearningeveryday.model;

public class WriteTutorialClass {
	int typeID;
	int ID;
	String imgName;
	String comment;
	
	public WriteTutorialClass(){}
	
	public WriteTutorialClass(int typeID, int ID, String imgName, String comment){
		this.typeID=typeID;
		this.ID=ID;
		this.imgName=imgName;
		this.comment=comment;
	}
	
	public WriteTutorialClass(int typeID,String imgName, String comment){
		this.typeID=typeID;
		this.imgName=imgName;
		this.comment=comment;
	}
	
	public void setTypeID(int typeID) {
		this.typeID=typeID;
	}
	
	public int getTypeID(){
		return typeID;
	}
	
	public void setID(int ID) {
		this.ID=ID;
	}
	
	public int getID(){
		return ID;
	}
	
	public void setImgName(String imgName){
		this.imgName=imgName;
	}
	
	public String getImgName(){
		return imgName;
	}
	
	public void setComment(String comment){
		this.comment=comment;
	}
	
	public String getComment(){
		return comment;
	}
}
