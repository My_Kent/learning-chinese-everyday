package com.nott.cs.chineselearningeveryday.model;

/**
 * All JUnit tests passed
 * 
 * @author Martin Handley mxh22u
 * @author Vm Tung txv03u
 * 
 */
public class StrokeWriteInfoClass {
	int wordID;
	int strokeID;
	int strokeOrder;
	VectorClass vector;

	public StrokeWriteInfoClass() {

	}

	public StrokeWriteInfoClass(int wordID, int strokeID, int strokeOrder, float startX, float startY, float endX, float endY) {
		this.wordID = wordID;
		this.strokeID=strokeID;
		this.strokeOrder = strokeOrder;
		vector = new VectorClass(startX, startY, endX, endY);
	}

	public void setWordID(int wordID) {
		this.wordID = wordID;
	}
	
	public int getWordID(){
		return wordID;
	}
	
	public void setStrokeID(int strokeID){
		this.strokeID=strokeID;
	}

	public int getStrokeID(){
		return strokeID;
	}
	
	public void setStrokeOrder(int strokeOrder) {
		this.strokeOrder = strokeOrder;
	}
	
	public int getStrokeOrder(){
		return strokeOrder;
	}
	

	public float getStartX() {
		return vector.getStartX();
	}

	public float getStartY() {
		return vector.getStartY();
	}

	public float getEndX() {
		return vector.getEndX();
	}

	public float getEndY() {
		return vector.getEndY();
	}

	public VectorClass getVectorClass() {
		return vector;
	}
}
