package com.nott.cs.chineselearningeveryday.model;

/**
 * All JUnit tests passed
 * 
 * @author Martin Handley mxh22u
 * @author Vm Tung txv03u
 * 
 */
public class ScenarioClass {

	int type_id = 0;
	int scenario_id = 0;
	String img;

	public ScenarioClass() {

	}

	public ScenarioClass(int type_id, String img) {
		this.type_id = type_id;
		this.img = img;
	}

	public ScenarioClass(int type_id, int scenario_id, String img) {
		this.type_id = type_id;
		this.scenario_id = scenario_id;
		this.img = img;
	}

	public void setTypeId(int type_id) {
		this.type_id = type_id;
	}

	public int getTypeId() {
		return type_id;
	}

	public void setScenarioId(int scenario_id) {
		this.scenario_id = scenario_id;
	}

	public int getScenarioId() {
		return scenario_id;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getImg() {
		return img;
	}
}
