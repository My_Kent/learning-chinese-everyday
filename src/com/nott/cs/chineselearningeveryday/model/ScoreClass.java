package com.nott.cs.chineselearningeveryday.model;

/**
 * All JUnit tests passed
 * 
 * @author Martin Handley mxh22u
 * @author Vm Tung txv03u
 * 
 */
public class ScoreClass {
	int scoreID = 0;
	int typeID = 0;
	float score;

	public ScoreClass() {

	}

	public ScoreClass(int scoreID, int typeID, float score) {
		this.scoreID = scoreID;
		this.typeID = typeID;
		this.score = score;
	}

	public ScoreClass(int typeID, float score) {
		this.typeID = typeID;
		this.score = score;
	}

	public void setScoreID(int scoreID) {
		this.scoreID = scoreID;
	}

	public int getScoreID() {
		return scoreID;
	}

	public void setTypeID(int typeID) {
		this.typeID = typeID;
	}

	public int getTypeID() {
		return typeID;
	}

	public void setScore(float score) {
		this.score = score;
	}

	public float getScore() {
		return score;
	}
}
