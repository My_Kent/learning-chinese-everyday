package com.nott.cs.chineselearningeveryday.model;

public class ProTutorialClass {
	int typeID;
	int ID;
	String alphabet;
	String audio;
	String example;
	
	public ProTutorialClass(){}
	
	public ProTutorialClass(int typeID, int ID, String alphabet, String audio, String example){
		this.typeID=typeID;
		this.ID=ID;
		this.alphabet=alphabet;
		this.audio=audio;
		this.example=example;
	}
	
	public ProTutorialClass(int typeID,String alphabet, String audio, String example){
		this.typeID=typeID;
		this.alphabet=alphabet;
		this.audio=audio;
		this.example=example;
	}
	
	public void setTypeID(int typeID) {
		this.typeID=typeID;
	}
	
	public int getTypeID(){
		return typeID;
	}
	
	public void setID(int ID) {
		this.ID=ID;
	}
	
	public int getID(){
		return ID;
	}
	
	public void setAlphabet(String alphabet){
		this.alphabet=alphabet;
	}
	
	public String getAlphabet(){
		return alphabet;
	}
	
	public void setAudio(String audio){
		this.audio=audio;
	}
	
	public String getAudio(){
		return audio;
	}
	
	public void setExample(String example){
		this.example=example;
	}
	
	public String getExample(){
		return example;
	}
	
}
