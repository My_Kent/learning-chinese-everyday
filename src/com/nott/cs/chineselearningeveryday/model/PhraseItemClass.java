package com.nott.cs.chineselearningeveryday.model;

public class PhraseItemClass {
	int typeID;
	int phraseID;
	String phrase;
	String pinyin;
	String meaning;
	String audio;
	boolean favourite;
	
	public PhraseItemClass(){}
	
	public PhraseItemClass(int typeID, String phrase, String pinyin, String meaning, String audio, boolean favourite){
		this.typeID=typeID;
		this.phrase=phrase;
		this.pinyin=pinyin;
		this.meaning=meaning;
		this.audio=audio;
		this.favourite=favourite;
	}
	
	public PhraseItemClass(int typeID, int phraseID, String phrase, String pinyin, String meaning, String audio, boolean favourite){
		this.typeID=typeID;
		this.phraseID=phraseID;
		this.phrase=phrase;
		this.pinyin=pinyin;
		this.meaning=meaning;
		this.audio=audio;
		this.favourite=favourite;
	}
	
	public void setTypeID(int typeID){
		this.typeID=typeID;
	}
	
	public int getTypeID(){
		return typeID;
	}
	
	public void setPhraseID(int phraseID){
		this.phraseID=phraseID;
	}
	
	public int getPhraseID(){
		return phraseID;
	}
	
	public void setPhrase(String phrase){
		this.phrase=phrase;
	}
	
	public String getPhrase(){
		return phrase;
	}
	
	public void setPinyin(String pinyin){
		this.pinyin=pinyin;
	}
	
	public String getPinyin(){
		return pinyin;
	}
	public void setMeaning(String meaning){
		this.meaning=meaning;
	}
	
	public String getMeaning(){
		return meaning;
	}
	

	public void setAudio(String audio){
		this.audio=audio;
	}
	
	public String getAudio(){
		return audio;
	}
	public void setFavourite(boolean favourite){
		this.favourite=favourite;
	}
	
	public boolean getFavourite(){
		return favourite;
	}
	
	
}
