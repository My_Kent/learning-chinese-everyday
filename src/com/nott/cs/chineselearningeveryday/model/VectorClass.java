package com.nott.cs.chineselearningeveryday.model;

//Martin - added default values for coordinates for use with default constructor, 
//Martin - Java will initialise to zero anyway

/**
 * Constraints of the class: Any value used in the class must have |value| <
 * Float.MAX_VALUE/4 to avoid overflow errors (Infinity) JUnit test case
 * confirms expected overflow
 * 
 * @author Martin Handley (mxh22u)
 * @author Vm Tung txv03u
 * 
 */
public class VectorClass {
	float startX = 0.0f;
	float startY = 0.0f;
	float endX = 0.0f;
	float endY = 0.0f;

	public VectorClass() {
	}

	public VectorClass(float startX, float startY, float endX, float endY) {
		this.startX = startX;
		this.startY = startY;
		this.endX = endX;
		this.endY = endY;
	}

	public float getStartX() {
		return startX;
	}

	public float getStartY() {
		return startY;
	}

	public float getEndX() {
		return endX;
	}

	public float getEndY() {
		return endY;
	}

	public float getVectX() {
		return endX - startX;
	}

	public float getVectY() {
		return endY - startY;
	}

	public float getLength() {
		return (float) Math.sqrt(Math.pow(endX - startX, 2) + Math.pow(endY - startY, 2));
		// new vect x = ratio^2 old vect x =>
		// newendx-startx=ratio^2*endx-ratio^2*startx => newendx =
		// ratio^2*endx-(ratio^2-1)startx
	}

}
