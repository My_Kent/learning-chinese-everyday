package com.nott.cs.chineselearningeveryday.model;

/**
 * All JUnit tests passed
 * @author Martin Handley mxh22u
 * @author Vm Tung txv03u
 *
 */

public class WordsClass {
	int wordID = 0;
	String word;
	String pinyin;
	String eng;
	int strokeNum = 0;

	public WordsClass() {
	}

	public WordsClass(int wordID, String word, String pinyin, String eng, int strokeNum) {
		this.wordID = wordID;
		this.word = word;
		this.pinyin=pinyin;
		this.eng=eng;
		this.strokeNum = strokeNum;
	}

	public WordsClass(String word, String pinyin, String eng, int strokeNum) {
		this.word = word;
		this.pinyin=pinyin;
		this.eng=eng;
		this.strokeNum = strokeNum;
	}

	public void setWordID(int wordID) {
		this.wordID = wordID;
	}

	public int getWordID() {
		return wordID;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public String getWord() {
		return word;
	}
	
	public void setPinyin(String pinyin) {
		this.pinyin = pinyin;
	}

	public String getPinyin() {
		return pinyin;
	}
	
	public void setEng(String eng) {
		this.eng = eng;
	}

	public String getEng() {
		return eng;
	}

	public void setStrokeNum(int strokeNum) {
		this.strokeNum = strokeNum;
	}

	public int getStrokeNum() {
		return strokeNum;
	}

}
