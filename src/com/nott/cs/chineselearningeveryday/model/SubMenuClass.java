package com.nott.cs.chineselearningeveryday.model;

/**
 * All JUnit tests passed
 * 
 * @author Martin Handley mxh22u
 * @author Vm Tung txv03u
 */
public class SubMenuClass {
	int id;
	String name;
	String img;
	String descriptions;

	public SubMenuClass() {

	}

	public SubMenuClass(String name, String img, String descriptions) {
		this.name = name;
		this.img = img;
		this.descriptions = descriptions;
	}

	public SubMenuClass(int id, String name, String img, String descriptions) {
		this.name = name;
		this.id = id;
		this.img = img;
		this.descriptions = descriptions;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getImg() {
		return img;
	}

	public void setDescriptions(String descriptions) {
		this.descriptions = descriptions;
	}

	public String getDescriptions() {
		return descriptions;
	}
}
