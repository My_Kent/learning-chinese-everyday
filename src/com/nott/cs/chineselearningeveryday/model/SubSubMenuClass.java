package com.nott.cs.chineselearningeveryday.model;

public class SubSubMenuClass {
	int typeID;
	String title;
	String description;
	
	public SubSubMenuClass(){
	}
	
	public SubSubMenuClass(int typeID, String sectionName, String description){
		this.typeID=typeID;
		this.title=sectionName;
		this.description=description;
	}
	
	public SubSubMenuClass(String sectionName, String description){
		this.title=sectionName;
		this.description=description;
	}
	
	public void setTypeID(int typeID) {
		this.typeID=typeID;
	}
	
	public int getTypeID(){
		return typeID;
	}
	
	public void setTitle(String sectionName){
		this.title=sectionName;
	}
	
	public String getTitle(){
		return title;
	}
	
	public void setDescription(String description) {
		this.description=description;
	}
	
	public String getDescription(){
		return description;
	}
	
}
