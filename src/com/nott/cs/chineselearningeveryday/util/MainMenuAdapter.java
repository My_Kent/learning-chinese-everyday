package com.nott.cs.chineselearningeveryday.util;

/**
 *  @author Vm Tung txv03u
 */

import com.nott.cs.chineselearningeveryday.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MainMenuAdapter extends BaseAdapter {

	private Context context;
	private final String[] menuText;

	public MainMenuAdapter(Context context, String[] menuText) {
		this.context = context;
		this.menuText = menuText;
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View gridView;

		if (convertView == null) {

			gridView = new View(context);
			gridView = inflater.inflate(R.layout.main_menu_items, null);
			TextView textView = (TextView) gridView.findViewById(R.id.text);

			String text = menuText[position];
			textView.setText(text);
			ImageView flag = (ImageView) gridView.findViewById(R.id.icon);

			if (text.equals("Tutorial")) {
				flag.setImageResource(R.drawable.tutorial_menu_icon);
			} else if (text.equals("Phrase Book")) {
				flag.setImageResource(R.drawable.phrase_menu_icon);
			} else if (text.equals("Scenarios")) {
				flag.setImageResource(R.drawable.scenario_menu_icon);
			} else if (text.equals("Words")) {
				
				flag.setImageResource(R.drawable.words_icon);
			} else {
				flag.setImageResource(R.drawable.quiz_menu_icon);
			}

		} else {
			gridView = (View) convertView;
		}

		return gridView;
	}

	@Override
	public int getCount() {
		return menuText.length;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

}
