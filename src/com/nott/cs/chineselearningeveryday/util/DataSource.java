package com.nott.cs.chineselearningeveryday.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.nott.cs.chineselearningeveryday.model.ConversationLineClass;
import com.nott.cs.chineselearningeveryday.model.IntroClass;
import com.nott.cs.chineselearningeveryday.model.LastAccessClass;
import com.nott.cs.chineselearningeveryday.model.PhraseItemClass;
import com.nott.cs.chineselearningeveryday.model.ProTutorialClass;
import com.nott.cs.chineselearningeveryday.model.SubSubMenuClass;
import com.nott.cs.chineselearningeveryday.model.WordsClass;
import com.nott.cs.chineselearningeveryday.model.ScoreClass;
import com.nott.cs.chineselearningeveryday.model.StrokeWriteInfoClass;
import com.nott.cs.chineselearningeveryday.model.SubMenuClass;
import com.nott.cs.chineselearningeveryday.model.VectorClass;
import com.nott.cs.chineselearningeveryday.model.WriteTutorialClass;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class DataSource {

	//this class will be used by all class to get any data from the database
	
	public static final String TABLE_SCENARIOTYPES = "scenarioTypes";
	public static final String TABLE_QUIZTYPES = "quizTypes";
	public static final String TABLE_PHRASESBOOK = "phrasesBook";
	public static final String TABLE_TUTORIALTYPES = "tutorialTypes";
	public static final String TABLE_SCENARIO = "scenario";
	public static final String TABLE_CONVERSATION_LINE = "conversationLine";
	public static final String TABLE_PHRASES = "phrases";
	public static final String TABLE_WORDS = "words";
	public static final String TABLE_STROKE_WRITE_INFO	 = "strokeWriteInfo";
	public static final String TABLE_LAST_ACCESS = "lastAccess";
	public static final String TABLE_SCORES = "scores";
	public static final String TABLE_WRITE_T = "writeT";
	public static final String TABLE_WRITE_TUTORIAL = "writeTutorial";
	public static final String TABLE_PRO_T = "proT";
	public static final String TABLE_PRO_TUTORIAL = "proTutorial";
	public static final String TABLE_INTRO = "intro";
	
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_IMG = "img";
	public static final String COLUMN_DESCRIPTIONS = "descriptions";
	
	public static final String COLUMN_TYPE_ID = "typeID";
	public static final String COLUMN_SCENE_ID = "sceneID";
	
	public static final String COLUMN_LINE_ID = "lineID";
	public static final String COLUMN_ORDER_ID = "orderID";
	public static final String COLUMN_SPEAKER = "speaker";
	public static final String COLUMN_CHI_LINE = "chiLine";
	public static final String COLUMN_PINYIN_LINE = "pinyinLine";
	public static final String COLUMN_ENG_LINE = "engLine";
	
	public static final String COLUMN_PHRASE = "phrase";
	public static final String COLUMN_PINYIN = "pinyin";
	public static final String COLUMN_MEANING = "meaning";
	
	public static final String COLUMN_PHRASE_ID = "phraseID";
	public static final String COLUMN_AUDIO = "audio";
	public static final String COLUMN_FAVOURITE = "favourite";
	
	public static final String COLUMN_WORD_ID= "wordID";
	public static final String COLUMN_WORDS= "word";
	public static final String COLUMN_ENG= "eng";
	public static final String COLUMN_STROKE_NUM= "strokeNum";
	
	public static final String COLUMN_STROKE_ID= "strokeID";
	public static final String COLUMN_STROKE_ORDER= "strokeOrder";
	public static final String COLUMN_START_X= "startX";
	public static final String COLUMN_START_Y= "startY";
	public static final String COLUMN_END_X= "endX";
	public static final String COLUMN_END_Y= "endY";
	
	public static final String COLUMN_ACCESS_ORDER= "accessOrder";
	
	public static final String COLUMN_SCORE_ID= "scoreID";
	public static final String COLUMN_SCORE= "score";
	
	public static final String COLUMN_WORD = "word";
	public static final String COLUMN_STROKE_TOTAL = "strokeTotal";
	
	public static final String COLUMN_TITLE="title";
	
	public static final String COLUMN_ID="ID";
	public static final String COLUMN_ALPHABET="alphabet";
	public static final String COLUMN_EXAMPLE="example";
	
	public static final String COLUMN_COMMENT="comment";
	
	
	// Database fields
	private SQLiteDatabase database;
	private MySQLiteHelper dbHelper;
	private String[] allMainColumns = { COLUMN_TYPE_ID, COLUMN_NAME, COLUMN_IMG, COLUMN_DESCRIPTIONS };
	private String[] allConversationLineColumns = { COLUMN_TYPE_ID, COLUMN_LINE_ID, COLUMN_IMG, COLUMN_ORDER_ID, COLUMN_SPEAKER, COLUMN_CHI_LINE, COLUMN_PINYIN_LINE, COLUMN_ENG_LINE, COLUMN_AUDIO};
	private String[] allPhrasesColumns = {COLUMN_TYPE_ID, COLUMN_PHRASE_ID, COLUMN_PHRASE, COLUMN_PINYIN, COLUMN_MEANING, COLUMN_AUDIO, COLUMN_FAVOURITE};
	private String[] allWordsColumns = {COLUMN_WORD_ID, COLUMN_WORD, COLUMN_PINYIN, COLUMN_ENG, COLUMN_STROKE_NUM};
	private String[] allStrokeWriteInfoColumns = {COLUMN_WORD_ID, COLUMN_STROKE_ID, COLUMN_STROKE_ORDER, COLUMN_START_X, COLUMN_START_Y, COLUMN_END_X, COLUMN_END_Y};
	private String[] allLastAccessColumns = {COLUMN_ACCESS_ORDER, COLUMN_TYPE_ID};
	private String[] allScoresColumns = {COLUMN_SCORE_ID, COLUMN_TYPE_ID, COLUMN_SCORE};
	private String[] allSubSubMenuColumns = {COLUMN_TYPE_ID, COLUMN_TITLE, COLUMN_DESCRIPTIONS};
	private String[] allProTutorialColumns = {COLUMN_TYPE_ID, COLUMN_ID, COLUMN_ALPHABET, COLUMN_AUDIO, COLUMN_EXAMPLE};
	private String[] allWriteTutorialColumns = {COLUMN_TYPE_ID, COLUMN_ID, COLUMN_IMG, COLUMN_COMMENT};
	private String[] allIntroColumns = {COLUMN_ID, COLUMN_IMG};

	//----initialise, open and close the database----
	public DataSource(Context context) {
		dbHelper = new MySQLiteHelper(context);
		try {
			dbHelper.createDataBase();
			dbHelper.close();
		} catch (IOException ioe) {

			throw new Error("Unable to create database");

		}

		
	}

	public void open() throws SQLException {
		try {

			dbHelper.openDataBase();

		} catch (SQLException sqle) {

			throw sqle;

		}
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	//----this section is for create, delete and get all data of these tables: phrases, quizzes, and scenarios----
	//these 3 are similar to each other, just different in table name
	
	public int createParallaxTypes(SubMenuClass pvc, String tableName) {
		ContentValues values = new ContentValues();
		values.put(COLUMN_IMG, pvc.getImg());
		values.put(COLUMN_NAME, pvc.getName());
		values.put(COLUMN_DESCRIPTIONS, pvc.getDescriptions());
		int insertId = (int) database.insert(tableName, null,
				values);
		
		return insertId;
	}

	public void deleteParallaxTypes(SubMenuClass pvc, String tableName) {
		long id = pvc.getId();
		System.out.println("scenarios deleted with id: " + id);
		database.delete(tableName, COLUMN_TYPE_ID
				+ " = " + id, null);
	}

	public List<SubMenuClass> getAllParallaxTypes(String tableName) {
		List<SubMenuClass> ParallaxTypesList = new ArrayList<SubMenuClass>();

		Cursor cursor = database.query(tableName,
				allMainColumns, null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			
			SubMenuClass parallaxType = new SubMenuClass();
			parallaxType.setId(cursor.getInt(0));
			parallaxType.setName(cursor.getString(1));
			parallaxType.setImg(cursor.getString(2));
			parallaxType.setDescriptions(cursor.getString(3));
			
			ParallaxTypesList.add(parallaxType);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return ParallaxTypesList;
	}
	
	public SubMenuClass getParallaxTypesWithID(int id, String tableName) {
		

		Cursor cursor = database.query(tableName,
				allMainColumns, COLUMN_TYPE_ID+"="+id, null, null, null, null);

		cursor.moveToFirst();
			
			SubMenuClass parallaxType = new SubMenuClass();
			parallaxType.setId(cursor.getInt(0));
			parallaxType.setName(cursor.getString(1));
			parallaxType.setImg(cursor.getString(2));
			parallaxType.setDescriptions(cursor.getString(3));
		
		// make sure to close the cursor
		cursor.close();
		return parallaxType;
	}
	
	public int createScenarioTypes(SubMenuClass scenarioTypes) {
		return createParallaxTypes(scenarioTypes, TABLE_SCENARIOTYPES);
		
	}

	public void deleteScenarioTypes(SubMenuClass scenarioTypes) {
		deleteParallaxTypes(scenarioTypes, TABLE_SCENARIOTYPES);
	}

	public List<SubMenuClass> getAllScenarioTypes() {
		
		return getAllParallaxTypes(TABLE_SCENARIOTYPES);
	}
	
	public SubMenuClass getScenarioTypesWithID(int id) {
		return getParallaxTypesWithID(id, TABLE_SCENARIOTYPES);
	}
	
	public int createQuizTypes(SubMenuClass quizTypes) {
		return createParallaxTypes(quizTypes, TABLE_QUIZTYPES);
	}

	public void deleteQuizTypes(SubMenuClass quizTypes) {
		deleteParallaxTypes(quizTypes, TABLE_QUIZTYPES);
	}

	public List<SubMenuClass> getAllQuizTypes() {
		return getAllParallaxTypes(TABLE_QUIZTYPES);
	}
	
	public int createPhrasesBook(SubMenuClass phrasesBook) {
		return createParallaxTypes(phrasesBook, TABLE_PHRASESBOOK);
	}

	public void deletePhrasesBook(SubMenuClass phrasesBook) {
		deleteParallaxTypes(phrasesBook, TABLE_PHRASESBOOK);
	}
	
	public List<SubMenuClass> getAllPhrasesBook() {
		return getAllParallaxTypes(TABLE_PHRASESBOOK);
	}
	
	public List<SubMenuClass> getAllTutorialTypes() {
		return getAllParallaxTypes(TABLE_TUTORIALTYPES);
	}
	
	public SubMenuClass getParallaxTypesWithName(String name, String tableName) {
		

		Cursor cursor = database.query(tableName,
				allMainColumns, COLUMN_NAME+"="+name, null, null, null, null);

		cursor.moveToFirst();
			
			SubMenuClass parallaxType = new SubMenuClass();
			parallaxType.setId(cursor.getInt(0));
			parallaxType.setName(cursor.getString(1));
			parallaxType.setImg(cursor.getString(2));
			parallaxType.setDescriptions(cursor.getString(3));
		
		// make sure to close the cursor
		cursor.close();
		return parallaxType;
	}
	
	public SubMenuClass getScenarioTypesWithName (String name) {
		return getParallaxTypesWithName(name, TABLE_SCENARIOTYPES);
	}
	
	public List<ConversationLineClass> getAllConversationLineWithTypeID(int typeID) {
		List<ConversationLineClass> conversationLineList = new ArrayList<ConversationLineClass>();

		Cursor cursor = database.query(TABLE_CONVERSATION_LINE,
				allConversationLineColumns, COLUMN_TYPE_ID+"="+typeID, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			
			ConversationLineClass conversationLine = new ConversationLineClass();
			
			conversationLine.setSceneID(cursor.getInt(0));
			conversationLine.setLineID(cursor.getInt(1));
			conversationLine.setImg(cursor.getString(2));
			conversationLine.setOrderID(cursor.getInt(3));
			conversationLine.setSpeaker(cursor.getString(4));
			conversationLine.setChi(cursor.getString(5));
			conversationLine.setPinyin(cursor.getString(6));
			conversationLine.setEng(cursor.getString(7));
			conversationLine.setAudio(cursor.getString(8));
			
			conversationLineList.add(conversationLine);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return conversationLineList;
	}

	public List<PhraseItemClass> getAllPhraseItem() {
		List<PhraseItemClass> PhraseList = new ArrayList<PhraseItemClass>();

		Cursor cursor = database.query(TABLE_PHRASES,
				allPhrasesColumns, null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			
			PhraseItemClass phrase = new PhraseItemClass();
			
			phrase.setTypeID(cursor.getInt(0));
			phrase.setPhraseID(cursor.getInt(1));
			phrase.setPhrase(cursor.getString(2));
			phrase.setPinyin(cursor.getString(3));
			phrase.setMeaning(cursor.getString(4));
			phrase.setAudio(cursor.getString(5));
			phrase.setFavourite(cursor.getInt(6)==1);
			
			PhraseList.add(phrase);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return PhraseList;
	}
	
	public List<PhraseItemClass> getAllPhraseItemWithType(int typeID) {
		List<PhraseItemClass> PhraseList = new ArrayList<PhraseItemClass>();

		Cursor cursor = database.query(TABLE_PHRASES,
				allPhrasesColumns, COLUMN_TYPE_ID+"="+typeID, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			
			PhraseItemClass phrase = new PhraseItemClass();
			
			phrase.setTypeID(cursor.getInt(0));
			phrase.setPhraseID(cursor.getInt(1));
			phrase.setPhrase(cursor.getString(2));
			phrase.setPinyin(cursor.getString(3));
			phrase.setMeaning(cursor.getString(4));
			phrase.setAudio(cursor.getString(5));
			phrase.setFavourite(cursor.getInt(6)==1);
			
			PhraseList.add(phrase);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return PhraseList;
	}
	
	public PhraseItemClass getPhraseItemWithID(int phraseID) {

		Cursor cursor = database.query(TABLE_PHRASES,
				allPhrasesColumns, COLUMN_PHRASE_ID+"="+phraseID, null, null, null, null);

		cursor.moveToFirst();
		
			
		PhraseItemClass phrase = new PhraseItemClass();
		
		phrase.setTypeID(cursor.getInt(0));
		phrase.setPhraseID(cursor.getInt(1));
		phrase.setPhrase(cursor.getString(2));
		phrase.setPinyin(cursor.getString(3));
		phrase.setMeaning(cursor.getString(4));
		phrase.setAudio(cursor.getString(5));
		phrase.setFavourite(cursor.getInt(6)==1);
			
		
		// make sure to close the cursor
		cursor.close();
		return phrase;
	}
	
	public List<PhraseItemClass> getAllFavouritePhraseWithType(int typeID) {
		List<PhraseItemClass> PhraseList = new ArrayList<PhraseItemClass>();

		Cursor cursor = database.query(TABLE_PHRASES,
				allPhrasesColumns, COLUMN_TYPE_ID+"="+typeID+" AND "+COLUMN_FAVOURITE+"=1", null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			
			PhraseItemClass phrase = new PhraseItemClass();
			
			phrase.setTypeID(cursor.getInt(0));
			phrase.setPhraseID(cursor.getInt(1));
			phrase.setPhrase(cursor.getString(2));
			phrase.setPinyin(cursor.getString(3));
			phrase.setMeaning(cursor.getString(4));
			phrase.setAudio(cursor.getString(5));
			phrase.setFavourite(cursor.getInt(6)==1);
			
			PhraseList.add(phrase);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return PhraseList;
	}
	
	public List<PhraseItemClass> getAllFavouritePhrase() {
		List<PhraseItemClass> PhraseList = new ArrayList<PhraseItemClass>();

		Cursor cursor = database.query(TABLE_PHRASES,
				allPhrasesColumns, COLUMN_FAVOURITE+"=1", null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			
			PhraseItemClass phrase = new PhraseItemClass();
			
			phrase.setTypeID(cursor.getInt(0));
			phrase.setPhraseID(cursor.getInt(1));
			phrase.setPhrase(cursor.getString(2));
			phrase.setPinyin(cursor.getString(3));
			phrase.setMeaning(cursor.getString(4));
			phrase.setAudio(cursor.getString(5));
			phrase.setFavourite(cursor.getInt(6)==1);
			
			PhraseList.add(phrase);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return PhraseList;
	}
	
	public List<String> getAllStringFavouritePhraseWithType(int typeID) {
		List<String> PhraseList = new ArrayList<String>();

		Cursor cursor = database.query(TABLE_PHRASES,
				allPhrasesColumns, COLUMN_TYPE_ID+"="+typeID+" AND "+COLUMN_FAVOURITE+"=1", null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			
			
			cursor.getInt(0);
			cursor.getInt(1);
		
			PhraseList.add(cursor.getString(2));
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return PhraseList;
	}
	
	public void setPhraseFavourite(int phraseID, boolean favourite){
		ContentValues args = new ContentValues();
		int value=0;
		if (favourite) value=1;
		args.put(COLUMN_FAVOURITE, value);
		database.update(TABLE_PHRASES, args, COLUMN_PHRASE_ID+"="+phraseID, null);
	}
	
	public List<StrokeWriteInfoClass> getAllStrokeWriteInfo(){
		List<StrokeWriteInfoClass> vectorClassList = new ArrayList<StrokeWriteInfoClass>();

		Cursor cursor = database.query(TABLE_STROKE_WRITE_INFO,
				allStrokeWriteInfoColumns,null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			
			StrokeWriteInfoClass strokeWriteInfo = new StrokeWriteInfoClass (cursor.getInt(0),cursor.getInt(1),cursor.getInt(2)
					,cursor.getFloat(3),cursor.getFloat(4),cursor.getFloat(5),cursor.getFloat(6));
			
			
			vectorClassList.add(strokeWriteInfo);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return vectorClassList;
		
	}
	
	public List<StrokeWriteInfoClass> getAllStrokeWriteInfoWithWordID(int wordID){
		List<StrokeWriteInfoClass> strokeWriteInfoList = new ArrayList<StrokeWriteInfoClass>();

		Cursor cursor = database.query(TABLE_STROKE_WRITE_INFO,
				allStrokeWriteInfoColumns,COLUMN_WORD_ID+"="+wordID, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			
			StrokeWriteInfoClass strokeWriteInfo = new StrokeWriteInfoClass (cursor.getInt(0),cursor.getInt(1),cursor.getInt(2)
					,cursor.getFloat(3),cursor.getFloat(4),cursor.getFloat(5),cursor.getFloat(6));
			
			
			strokeWriteInfoList.add(strokeWriteInfo);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return strokeWriteInfoList;
		
	}
	
	public List<VectorClass> getAllVectorStrokeWriteInfoWithWordID(int wordID){
		List<VectorClass> strokeWriteInfoList = new ArrayList<VectorClass>();

		Cursor cursor = database.query(TABLE_STROKE_WRITE_INFO,
				allStrokeWriteInfoColumns,COLUMN_WORD_ID+"="+wordID, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			
			StrokeWriteInfoClass strokeWriteInfo = new StrokeWriteInfoClass (cursor.getInt(0),cursor.getInt(1),cursor.getInt(2)
					,cursor.getFloat(3),cursor.getFloat(4),cursor.getFloat(5),cursor.getFloat(6));
			
			
			strokeWriteInfoList.add(strokeWriteInfo.getVectorClass());
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return strokeWriteInfoList;
		
	}
	
	public List<LastAccessClass> getAllLastAccess(){
		List<LastAccessClass> lastAccessList = new ArrayList<LastAccessClass>();

		Cursor cursor = database.query(TABLE_LAST_ACCESS,
				allLastAccessColumns,null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			
			LastAccessClass lastAccess = new LastAccessClass ();
			
			lastAccess.setAccessOrder(cursor.getInt(0));
			lastAccess.setScenarioTypeID(cursor.getInt(1));
			
			lastAccessList.add(lastAccess);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return lastAccessList;
	}
	
	public void setLastAccess(int typeID){
		if (getAllLastAccess().size()==10) {
			Cursor cursor = database.query(TABLE_LAST_ACCESS,allLastAccessColumns, null, null, null, null, null); 

		    if(cursor.moveToFirst()) {
		        int rowId = cursor.getInt(0); 

		        database.delete(TABLE_LAST_ACCESS, COLUMN_ACCESS_ORDER +  "=" + rowId, null);
		   }
			
			//return insertId;
		}
		ContentValues values = new ContentValues();
		values.put(COLUMN_TYPE_ID, typeID);
		int insertId = (int) database.insert(TABLE_LAST_ACCESS, null,
				values);
	}
	
	public List<ScoreClass> getAllScore(){
		List<ScoreClass> scoreList = new ArrayList<ScoreClass>();

		Cursor cursor = database.query(TABLE_SCORES,
				allScoresColumns,null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			
			ScoreClass score = new ScoreClass();
			
			score.setScoreID(cursor.getInt(0));
			score.setTypeID(cursor.getInt(1));
			score.setScore(cursor.getFloat(2));
			
			scoreList.add(score);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return scoreList;
	}
	
	public List<ScoreClass> getAllScoreWithTypeID(int typeID){
		List<ScoreClass> scoreList = new ArrayList<ScoreClass>();

		Cursor cursor = database.query(TABLE_SCORES,
				allScoresColumns,COLUMN_TYPE_ID+"="+typeID, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			
			ScoreClass score = new ScoreClass();
			
			score.setScoreID(cursor.getInt(0));
			score.setTypeID(cursor.getInt(1));
			score.setScore(cursor.getFloat(2));
			
			scoreList.add(score);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return scoreList;
	}
	
	public int setScore(ScoreClass score) {
		ContentValues values = new ContentValues();
		values.put(COLUMN_TYPE_ID, score.getTypeID());
		values.put(COLUMN_SCORE, score.getScore());
		int insertId = (int) database.insert(TABLE_SCORES, null,
				values);
		
		return insertId;
	}
	
	public List<WordsClass> getAllWords(){
		List<WordsClass> WordsList = new ArrayList<WordsClass>();

		Cursor cursor = database.query(TABLE_WORDS,
				allWordsColumns,null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			
			WordsClass words = new WordsClass();
			
			words.setWordID(cursor.getInt(0));
			words.setWord(cursor.getString(1));
			words.setPinyin(cursor.getString(2));
			words.setEng(cursor.getString(3));
			words.setStrokeNum(cursor.getInt(4));
			
			WordsList.add(words);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return WordsList;
	}
	
	public WordsClass getWordsWithID(int wordID){
		

		Cursor cursor = database.query(TABLE_WORDS,
				allWordsColumns,COLUMN_WORD_ID+"="+wordID, null, null, null, null);

		cursor.moveToFirst();
		
		
		WordsClass words = new WordsClass();
		
		words.setWordID(cursor.getInt(0));
		words.setWord(cursor.getString(1));
		words.setPinyin(cursor.getString(2));
		words.setEng(cursor.getString(3));
		words.setStrokeNum(cursor.getInt(4));
		
		// make sure to close the cursor
		cursor.close();
		return words;
	}
	
	public List<SubSubMenuClass> getAllSubSubMenuClass(String tableName){
		List<SubSubMenuClass> subSubMenuList = new ArrayList<SubSubMenuClass>();

		Cursor cursor = database.query(tableName,
				allSubSubMenuColumns,null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			
			SubSubMenuClass subSubMenu = new SubSubMenuClass();
			
			subSubMenu.setTypeID(cursor.getInt(0));
			subSubMenu.setTitle(cursor.getString(1));
			subSubMenu.setDescription(cursor.getString(2));
			
			subSubMenuList.add(subSubMenu);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return subSubMenuList;
		
	}
	
	public List<SubSubMenuClass> getAllSubSubMenuProClass(){
		return getAllSubSubMenuClass(TABLE_PRO_T);
	}
	
	public List<SubSubMenuClass> getAllSubSubMenuWriteClass(){
		return getAllSubSubMenuClass(TABLE_WRITE_T);
	}
	
	public List<ProTutorialClass> getAllProTutorialClassWithTypeID(int typeID){
		List<ProTutorialClass> proTutorialList = new ArrayList<ProTutorialClass>();

		Cursor cursor = database.query(TABLE_PRO_TUTORIAL,
				allProTutorialColumns,COLUMN_TYPE_ID+"="+typeID, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			
			ProTutorialClass proTutorial = new ProTutorialClass();
			
			proTutorial.setTypeID(cursor.getInt(0));
			proTutorial.setID(cursor.getInt(1));
			proTutorial.setAlphabet(cursor.getString(2));
			proTutorial.setAudio(cursor.getString(3));
			proTutorial.setExample(cursor.getString(4));
			
			proTutorialList.add(proTutorial);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return proTutorialList;
		
	} 
	
	public List<WriteTutorialClass> getAllWriteTutorialClassWithTypeID(int typeID){
		List<WriteTutorialClass> writeTutorialList = new ArrayList<WriteTutorialClass>();

		Cursor cursor = database.query(TABLE_WRITE_TUTORIAL,
				allWriteTutorialColumns,COLUMN_TYPE_ID+"="+typeID, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			
			WriteTutorialClass writeTutorial = new WriteTutorialClass();
			
			writeTutorial.setTypeID(cursor.getInt(0));
			writeTutorial.setID(cursor.getInt(1));
			writeTutorial.setImgName(cursor.getString(2));
			writeTutorial.setComment(cursor.getString(3));
			
			writeTutorialList.add(writeTutorial);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return writeTutorialList;
		
	} 
	
	public List<IntroClass> getAllIntroClass(){
		List<IntroClass> introList = new ArrayList<IntroClass>();

		Cursor cursor = database.query(TABLE_INTRO,
				allIntroColumns,null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			
			IntroClass intro = new IntroClass();
			
			
			intro.setID(cursor.getInt(0));
			intro.setImg(cursor.getString(1));
			
			introList.add(intro);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return introList;
		
	} 
	
}
