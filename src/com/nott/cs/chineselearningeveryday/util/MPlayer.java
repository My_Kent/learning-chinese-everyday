package com.nott.cs.chineselearningeveryday.util;

import android.content.Context;
import android.media.MediaPlayer;

/**
 * @author Vm Tung txv03u
 */

public class MPlayer {

	private static MediaPlayer mediaPlayer = new MediaPlayer();
	
	public static void playSound(Context context, int resid) {
		
		mediaPlayer.stop();
		mediaPlayer = MediaPlayer.create(context, resid);
		mediaPlayer.start();
		
	}	
}
