package com.nott.cs.chineselearningeveryday;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.nott.cs.chineselearningeveryday.VerticalPager;
import com.nott.cs.chineselearningeveryday.model.PhraseItemClass;
import com.nott.cs.chineselearningeveryday.model.ProTutorialClass;
import com.nott.cs.chineselearningeveryday.model.WordsClass;
import com.nott.cs.chineselearningeveryday.model.ScoreClass;
import com.nott.cs.chineselearningeveryday.model.WriteTutorialClass;
import com.nott.cs.chineselearningeveryday.util.DataSource;
import com.nott.cs.chineselearningeveryday.util.MPlayer;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnClickListener;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TableLayout;
import android.widget.TextView;

public class VerticalFragment extends Fragment {
	public static final String ITEM_TYPE="item_type";
	public static final String PARENT_TYPE_ID="parent_type_id";
	public static final String SPECIAL_REQUEST="special_request";
	int created = 0;
	FragmentListener mCallback;
	DataSource datasource;
	
	LayoutInflater inflater;
	ViewGroup container;
	
	int item_type;
	int parent_type_id;
	int special_request;
	VerticalPager verticalPager;
	int score=0;
	int currentPage=0;

	int unit=0;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		this.inflater=inflater;
		this.container=container;
		
		//created=1;
		datasource = new DataSource(getActivity());
		datasource.open();
		
		item_type = getArguments().getInt(ITEM_TYPE);
		parent_type_id = getArguments().getInt(PARENT_TYPE_ID);
		special_request = getArguments().getInt(SPECIAL_REQUEST);
		
		View rootView = inflater.inflate(R.layout.vertical_fragment, container,
				false);
		verticalPager = (VerticalPager)rootView.findViewById(R.id.activity_main_vertical_pager);
		if (item_type==1) {
			if (parent_type_id==2){
				initProTutView();
				//init pronounce tut
			} else {
				initWriteTutView();
				//init write tut
			}
		} else if (item_type==3){
			initPhraseView();
		} else if (item_type==5) {
			
			currentPage=0;
			verticalPager.setPagingEnabled(false);
			
			//data retrieve parent id
			if (parent_type_id==1) {
				initQuizOrderView();
			} else if (parent_type_id==2) {
				initQuizStrokeNumberView();
			}
		}
		datasource.close();
		return rootView;
	}
	
	public class MyOnClick implements OnClickListener{

		String answer;
		public MyOnClick(String answer){
			this.answer=answer;
		}
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v.getTag()==null) {

				verticalPager.snapToPage(++currentPage);
			} else if (v.getTag().equals("finish")) {
				mCallback.onMenuItemSelected(5);
			} else {
				verticalPager.snapToPage(++currentPage);
				if (v.getTag().equals("ins"))
					runnable.run();
				
			}
			//verticalPager.setCurrentPage(1);
			//update score to database
			if (v.getId()!=View.NO_ID) {
				final Dialog infoDialog = new Dialog(getActivity());
				infoDialog.setTitle("Score");
				infoDialog.setContentView(R.layout.score_dialog);
				TextView scoreView = (TextView) infoDialog.findViewById(R.id.score);
				scoreView.setText("the correct answer is: \n"+answer+"\nyour score: "+score*1.0/v.getId()*10);
				scoreView.setTextSize(25);
				Button bttOK= (Button) infoDialog.findViewById(R.id.OK_btt);
				bttOK.setOnClickListener(new OnClickListener(){
				    @Override
				    public void onClick(View v) {
				    	
				        infoDialog.dismiss();
				    }
				});
				infoDialog.show();
				
				
				datasource.open();
				datasource.setScore(new ScoreClass(parent_type_id, (float) (score*1.0/v.getId()*10)));
				datasource.close();
			}
			
			
			score=0;
		}
		
	}
	
	public class MyOnTouch implements OnTouchListener {

		
		
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);
			    	v.startDrag(null, shadowBuilder, v, 0);
			    v.setVisibility(View.INVISIBLE);
			    return true;
			} else {
			    return false;
			}
		}
		
	}

	public class MyOnDrag implements OnDragListener {
		
		LayoutInflater inflater;
		ViewGroup container;
		public MyOnDrag(LayoutInflater inflater, ViewGroup container) {
			this.inflater=inflater;
			this.container=container;
		}
		
		public MyOnDrag(){}
		
		@Override
		public boolean onDrag(View v, DragEvent event) {
			if (event.getAction()==DragEvent.ACTION_DROP) {
				if (v.getTag()==null) {
					View view = (View) event.getLocalState();
					view.setVisibility(View.VISIBLE);
				} else if (v.getTag().equals("strokeAnswer")){
					View view = (View) event.getLocalState();
					ViewGroup from = (ViewGroup) view.getParent();
					if (from.getTag()!=null) from.removeView(view);
					else {
						Log.w("yes","why");
						view.setVisibility(View.VISIBLE);
						int strokeTotal = view.getId();
						view = inflater.inflate(R.layout.quiz_box_item, container,
								false);
						TextView tv = (TextView) view.findViewById(R.id.quiz_item);
						tv.setText(""+strokeTotal);
						view.setId(strokeTotal);
						view.setOnTouchListener(new MyOnTouch());
					}
					FrameLayout to = (FrameLayout) v;
					if (to.getChildCount()!=0){
						View child = to.getChildAt(0);
						to.removeView(child);
						if (from.getTag()!=null) from.addView(child);
						if (from.getId()==child.getId()) score++;
						if (to.getId()==child.getId()) score--;
						
					}
					to.addView(view);
					if (to.getId()==view.getId()) score++;
					if (from.getId()==view.getId()) score--;
					view.setVisibility(View.VISIBLE);
				}
				else {
					View view = (View) event.getLocalState();
					ViewGroup from = (ViewGroup) view.getParent();
					from.removeView(view);
					FrameLayout to = (FrameLayout) v;
					if (to.getChildCount()!=0){
						View child = to.getChildAt(0);
						to.removeView(child);
						from.addView(child);
						if (from.getId()==child.getId()) score++;
						if (to.getId()==child.getId()) score--;
						
					}
					to.addView(view);
					if (to.getId()==view.getId()) score++;
					if (from.getId()==view.getId()) score--;
					view.setVisibility(View.VISIBLE);
				}
				System.out.println("score: "+score);
			}
			return true;
		}
		
	}
	
	public void initProTutView() {

		List<ProTutorialClass> proTutorialClassList = new ArrayList<ProTutorialClass>();
		
		proTutorialClassList = datasource.getAllProTutorialClassWithTypeID(special_request);
		
		int maxItemPage=5;
		int done=0;
		int countView=0;
		int current=0;
		while(true){
			if (done==1) break;
			LinearLayout ll = (LinearLayout) inflater.inflate(R.layout.phrase_book_page, container,
					false);
			countView=0;
			while(countView<maxItemPage) {
				
					//if (special_request>=0) 
					//	current=special_request;
					
					//getting all the required parameters
					int ID = proTutorialClassList.get(current).getID();
					String alphabet=proTutorialClassList.get(current).getAlphabet();
					String audio=proTutorialClassList.get(current).getAudio();
					String example=proTutorialClassList.get(current).getExample();
					
					
					//putting all the parameters into 1 item view
					LinearLayout llItem = (LinearLayout) inflater.inflate(R.layout.pro_tutorial_item, container,
							false);
					llItem.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1f));
					TextView tv = (TextView) llItem.findViewById(R.id.alphabet);
					tv.setText(alphabet);
					tv = (TextView) llItem.findViewById(R.id.example);
					tv.setText(example);
					
					//setting up button and favourite combobox
					llItem.setTag(audio);
					llItem.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							//mCallback.playSound((String) v.getTag());
							//updated media player to a static class
							MPlayer.playSound(getActivity(), getResources().getIdentifier((String) v.getTag(), "raw", getActivity().getPackageName()));
					    }
					});
					
										//count number of view on the page
					countView++;
					
					llItem.setBackgroundResource(R.drawable.border);
					//add item to 1 page
					ll.addView(llItem);
					current++;
					
					if (current==proTutorialClassList.size()) {
						for (int i=0; i<maxItemPage-countView; i++) {
							LinearLayout llItemEmpty = (LinearLayout) inflater.inflate(R.layout.phrase_book_empty_item, container,
									false);
							
							llItemEmpty.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1f));
							ll.addView(llItemEmpty);
							
						}
						countView=maxItemPage;
						done=1;
					}
				
			}
			//no view then not add the the total pager
			if (countView!=0)
				verticalPager.addView(ll);
		}
		
	}

	public void initWriteTutView() {

		List<WriteTutorialClass> writeTutorialClassList = new ArrayList<WriteTutorialClass>();
		
		writeTutorialClassList = datasource.getAllWriteTutorialClassWithTypeID(special_request);
		
		int maxItemPage=5;
		int done=0;
		int countView=0;
		int current=0;
		while(true){
			if (done==1) break;
			LinearLayout ll = (LinearLayout) inflater.inflate(R.layout.phrase_book_page, container,
					false);
			countView=0;
			while(countView<maxItemPage) {
				
					//if (special_request>=0) 
					//	current=special_request;
					
					//getting all the required parameters
					int ID = writeTutorialClassList.get(current).getID();
					String img=writeTutorialClassList.get(current).getImgName();
					String comment=writeTutorialClassList.get(current).getComment();
					
					
					//putting all the parameters into 1 item view
					LinearLayout llItem = (LinearLayout) inflater.inflate(R.layout.write_tutorial_item, container,
							false);
					llItem.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1f));
					TextView tv = (TextView) llItem.findViewById(R.id.comment);
					tv.setText(comment);
					
					ImageView iv = (ImageView) llItem.findViewById(R.id.image);
					
					iv.setImageResource(getResources().getIdentifier(img, "drawable", getActivity().getPackageName()));
					
										//count number of view on the page
					countView++;
					llItem.setBackgroundResource(R.drawable.border);
					//add item to 1 page
					ll.addView(llItem);
					current++;
					
					if (current==writeTutorialClassList.size()) {
						for (int i=0; i<maxItemPage-countView; i++) {
							LinearLayout llItemEmpty = (LinearLayout) inflater.inflate(R.layout.phrase_book_empty_item, container,
									false);
							
							llItemEmpty.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1f));
							ll.addView(llItemEmpty);
							
						}
						countView=maxItemPage;
						done=1;
					}
				
			}
			//no view then not add the the total pager
			if (countView!=0)
				verticalPager.addView(ll);
		}
		
	}

	
	public void initPhraseView() {

		List<PhraseItemClass> phraseItemClassList = new ArrayList<PhraseItemClass>();
		if (special_request>=0) phraseItemClassList.add(datasource.getPhraseItemWithID(special_request));
		else phraseItemClassList = datasource.getAllPhraseItemWithType(parent_type_id);
		List<String> favouriteList = new ArrayList<String>();
		int done=0;
		int countView=0;
		int current=0;
		while(true){
			if (done==1) break;
			LinearLayout ll = (LinearLayout) inflater.inflate(R.layout.phrase_book_page, container,
					false);
			countView=0;
			while(countView<3) {
				
					//getting all the required parameters
					int phraseID = phraseItemClassList.get(current).getPhraseID();
					String engPhrase=phraseItemClassList.get(current).getMeaning();
					String chiPhrase=phraseItemClassList.get(current).getPhrase();
					String pinyin=phraseItemClassList.get(current).getPinyin();
					String speak=phraseItemClassList.get(current).getAudio();
					boolean favourite=phraseItemClassList.get(current).getFavourite();
					
					
					//putting all the parameters into 1 item view
					LinearLayout llItem = (LinearLayout) inflater.inflate(R.layout.phrase_book_item, container,
							false);
					llItem.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1f));
					TextView tv = (TextView) llItem.findViewById(R.id.eng_phrase);
					tv.setText(engPhrase);
					tv = (TextView) llItem.findViewById(R.id.chi_phrase);
					tv.setText(chiPhrase.replaceAll("\\|", ""));
					tv = (TextView) llItem.findViewById(R.id.pinyin);
					tv.setText(pinyin);
					
					//setting up button and favourite combobox
					llItem.setTag(speak);
					llItem.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							//mCallback.playSound((String) v.getTag());
							//updated media player to a static class
							MPlayer.playSound(getActivity(), getResources().getIdentifier((String) v.getTag(), "raw", getActivity().getPackageName()));
					    }
					});
					
					CheckBox cb = (CheckBox) llItem.findViewById(R.id.favourite);
					System.out.println(current+" "+phraseItemClassList.get(current).getFavourite()+" "+favourite);
					cb.setChecked(favourite);
					if (favourite) favouriteList.add(chiPhrase);
					cb.setId(phraseID);
					cb.setTag(parent_type_id);
					//cb.seton
					cb.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				        @Override
				        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				            //do stuff
				        	DataSource datasource = new DataSource(getActivity());
				    		datasource.open();
				        	datasource.setPhraseFavourite(buttonView.getId(), isChecked);
				        	datasource.close();
				        	mCallback.showRightMenu(RightMenuFragment.SUB_MENU_PHRASES);
				        	
				        }
				    });
					//count number of view on the page
					countView++;
					llItem.setBackgroundResource(R.drawable.border);
					//add item to 1 page
					ll.addView(llItem);
					current++;
					
					if (current==phraseItemClassList.size()) {
						for (int i=0; i<3-countView; i++) {
							LinearLayout llItemEmpty = (LinearLayout) inflater.inflate(R.layout.phrase_book_empty_item, container,
									false);
							
							llItemEmpty.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1f));
							ll.addView(llItemEmpty);
							
						}
						countView=3;
						done=1;
					}
				
			}
			//no view then not add the the total pager
			if (countView!=0)
				verticalPager.addView(ll);
		}
		mCallback.showRightMenu(RightMenuFragment.SUB_MENU_PHRASES);
	}
	
	Handler handler = new Handler();
	TextView countText;
	int countdown=10;
	Runnable runnable = new Runnable() {
		@Override
		public void run() {
			
			/* do what you need to do */
			countText.setText(""+countdown--);
			
			/* and here comes the "trick" */
			if (countdown>=0)
				handler.postDelayed(this, 1000);
			else verticalPager.snapToPage(++currentPage);
		}
	};
	
	public void initQuizOrderView() {
		
		// show 5 random phrase
		LinearLayout instructionLayout = (LinearLayout) inflater.inflate(R.layout.quiz_instructions, container,
				false);
		TextView title = (TextView) instructionLayout.findViewById(R.id.ins_title);
		title.setText(R.string.order_quiz_ins_title);
		TextView content = (TextView) instructionLayout.findViewById(R.id.ins_content);
		content.setText(R.string.order_quiz_ins_cont);
		
		Button startBtt = (Button) instructionLayout.findViewById(R.id.start_quiz_btt);
		startBtt.setId(View.NO_ID);
		startBtt.setTag("ins");
		startBtt.setOnClickListener(new MyOnClick(""));
		
		verticalPager.addView(instructionLayout);
		
		Random r = new Random();
		List<PhraseItemClass> phraseItemList = datasource.getAllPhraseItem();
		int maxquiz=5;
		List <Integer> randomPhraseID = new ArrayList<Integer>();
		LinearLayout lpage = (LinearLayout) inflater.inflate(R.layout.quiz_order_pre, container,
				false);
		for (int i=0; i<maxquiz && i<phraseItemList.size(); i++) {
			int temp;
			do {
				temp=r.nextInt(phraseItemList.size());
				
			} while (randomPhraseID.contains(temp) || phraseItemList.get(temp).getPhrase().split("\\|").length==1 
					|| phraseItemList.get(temp).getPhrase().contains("/"));
			
			randomPhraseID.add(temp);
			LinearLayout ll = (LinearLayout) inflater.inflate(R.layout.quiz_order_phrase_item, container,
					false);
			
			TextView chiPhrase = (TextView) ll.findViewById(R.id.chiPhrase);
			TextView pinyinPhrase = (TextView) ll.findViewById(R.id.pinyinPhrase);
			TextView engPhrase = (TextView) ll.findViewById(R.id.engPhrase);
			
			chiPhrase.setText(phraseItemList.get(temp).getPhrase().replaceAll("\\|", ""));
			
			pinyinPhrase.setText(phraseItemList.get(temp).getPinyin());
			engPhrase.setText(phraseItemList.get(temp).getMeaning());
			
			ll.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1f));
			ll.setPadding(20, 20, 20, 20);
			
			
			lpage.addView(ll,0);
			
			
		}
		
		countText = (TextView) lpage.findViewById(R.id.countdown);
		countText.setPadding(20, 20, 20, 20);
		//countText.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1f));
		
		//runnable.run();
		
		
		verticalPager.addView(lpage);
		int sizetemp = randomPhraseID.size();
		//System.out.println("max: "+maxquiz);
		for (int i=0; i<sizetemp; i++) {
			
			int temp;
				temp=r.nextInt(randomPhraseID.size());
			
			PhraseItemClass currentItem = phraseItemList.get(randomPhraseID.get(temp));
			String sentence = phraseItemList.get(randomPhraseID.get(temp)).getPhrase();
			randomPhraseID.remove(temp);
			//phraseItemList.remove(i);
			
			LinearLayout ll = (LinearLayout) inflater.inflate(R.layout.quiz_page, container,
					false);
			ll.setOnDragListener(new MyOnDrag());
			LinearLayout ltop= (LinearLayout) ll.findViewById(R.id.from_area);
			LinearLayout lbot= (LinearLayout) ll.findViewById(R.id.to_area);
			
			
			Button bttNext = (Button) ll.findViewById(R.id.next_quiz);
			bttNext.setId(0);
			if (i+1==maxquiz) {
				bttNext.setText("finish");
				bttNext.setTag("finish");
			}
			bttNext.setOnClickListener(new MyOnClick(sentence.replaceAll("\\|", "")));
			
			LinearLayout llHint = (LinearLayout) ll.findViewById(R.id.hint_layout);
			Button bttHint = (Button) ll.findViewById(R.id.show_hint);
			
			TextView tvHint1 = (TextView) llHint.findViewById(R.id.hint1);
			TextView tvHint2 = (TextView) llHint.findViewById(R.id.hint2);
			tvHint1.setText(currentItem.getPinyin());
			tvHint1.setVisibility(View.GONE);
			tvHint2.setText(currentItem.getMeaning());
			tvHint2.setVisibility(View.GONE);
			
			bttHint.setId(i+2);
			bttHint.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					LinearLayout tempLL = (LinearLayout) verticalPager.getChildAt(v.getId());
					TextView tvHint1 = (TextView) tempLL.findViewById(R.id.hint1);
					TextView tvHint2 = (TextView) tempLL.findViewById(R.id.hint2);
					
					tvHint1.setVisibility(View.VISIBLE);
					tvHint2.setVisibility(View.VISIBLE);
					
				}
			});
			
			String text[] = sentence.split("\\|");
			System.out.println("senc: "+sentence+ " text length: "+text.length+"");
			
			bttNext.setId(text.length);
			for (int j=0; j<text.length; j++) {
				int i1=0;
				String current="done";
				do {
					i1 = r.nextInt(text.length);
					current = text[i1];
				} while(current.equals("done"));
				
				text[i1]="done";
				
				LinearLayout lboxContainer = (LinearLayout)inflater.inflate(R.layout.quiz_box, container,
						false); 
				FrameLayout lbox = (FrameLayout) lboxContainer.findViewById(R.id.box);
				
				lboxContainer.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1f));
				
				FrameLayout lboxItem = (FrameLayout) inflater.inflate(R.layout.quiz_box_item, container,
						false);
					
				TextView tv = (TextView) lboxItem.findViewById(R.id.quiz_item);
				tv.setText(current);
				
				
				lboxItem.setOnTouchListener(new MyOnTouch());
				lboxItem.setId(i1);
				
				lbox.addView(lboxItem);
				lbox.setTag("quizOrder");
				lbox.setOnDragListener(new MyOnDrag());
				ltop.addView(lboxContainer);
				
				LinearLayout lboxContainer2 = (LinearLayout)inflater.inflate(R.layout.quiz_box, container,
						false); 
				lboxContainer2.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1f));
				FrameLayout lbox2 = (FrameLayout) lboxContainer2.findViewById(R.id.box);
				
				lbox2.setTag("quizOrder");
				lbox2.setOnDragListener(new MyOnDrag());
				lbox2.setId(j);
				lbot.addView(lboxContainer2);
			}
			verticalPager.addView(ll);
		
		}
	}
	
	public void initQuizStrokeNumberView() {
		
		
		LinearLayout instructionLayout = (LinearLayout) inflater.inflate(R.layout.quiz_instructions, container,
				false);
		TextView title = (TextView) instructionLayout.findViewById(R.id.ins_title);
		title.setText(R.string.stroke_quiz_ins_title);
		TextView content = (TextView) instructionLayout.findViewById(R.id.ins_content);
		content.setText(R.string.stroke_quiz_ins_cont);
		
		Button startBtt = (Button) instructionLayout.findViewById(R.id.start_quiz_btt);
		startBtt.setId(View.NO_ID);
		//startBtt.setTag("ins");
		startBtt.setOnClickListener(new MyOnClick(""));
		
		verticalPager.addView(instructionLayout);
		
		
		Random r = new Random();
		List<WordsClass> quizStrokeList = datasource.getAllWords();
		
		int maxquiz=5;
		int maxitem=5;
		
		List <Integer> randomQuizID = new ArrayList<Integer>();
		
		for (int i=0; i<maxquiz && i<quizStrokeList.size(); i++) {
			
			
			LinearLayout ll = (LinearLayout) inflater.inflate(R.layout.quiz_page, container,
					false);
			ll.setOnDragListener(new MyOnDrag());
			LinearLayout ltop= (LinearLayout) ll.findViewById(R.id.from_area);
			LinearLayout lbot= (LinearLayout) ll.findViewById(R.id.to_area);
			
			ll.removeView(ll.findViewById(R.id.hint_layout));
			
			Button bttNext = (Button) ll.findViewById(R.id.next_quiz);
			bttNext.setId(1);
			if (i+1==maxquiz) {
				bttNext.setText("finish");
				bttNext.setTag("finish");
			}
			
			bttNext.setId(maxitem);
			
			List<Integer> answer = new ArrayList<Integer>();
			
			String correctAns="";
			for (int j=0; j<maxitem; j++) {
				
				int temp;
				do {
					temp=r.nextInt(quizStrokeList.size());
					
				} while (randomQuizID.contains(temp));
				
				randomQuizID.add(temp);
				
				String word = quizStrokeList.get(temp).getWord();
				int strokeTotal = quizStrokeList.get(temp).getStrokeNum();
				
				LinearLayout lbox = (LinearLayout) inflater.inflate(R.layout.quiz_stroke_number_item, container,
						false);
				
				lbox.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1f));
				//lbox.setLayoutParams(params);
					
				TextView tv = (TextView) lbox.findViewById(R.id.quiz_word);
				tv.setText(word);
				correctAns+=word+": "+strokeTotal+"\n";
				
				FrameLayout lboxItem = (FrameLayout) lbox.findViewById(R.id.quiz_box_correct);
				
				lboxItem.setId(strokeTotal);
				
				lboxItem.setOnDragListener(new MyOnDrag(inflater,container));
				
				lboxItem.setTag("strokeAnswer");
				
				ltop.addView(lbox);
				
				if (!answer.contains(strokeTotal)) {
					answer.add(strokeTotal);
					int addAt=-1;
					for (int k=0; k<lbot.getChildCount(); k++) {
						
						if (((FrameLayout)((LinearLayout)lbot.getChildAt(k)).getChildAt(0)).getChildAt(0).getId()>strokeTotal) {
							
							addAt=k;
							break;
						}
					}
					
					LinearLayout lboxContainer2 = (LinearLayout)inflater.inflate(R.layout.quiz_box, container,
							false); 
					lboxContainer2.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1f));
					FrameLayout lbox2 = (FrameLayout) lboxContainer2.findViewById(R.id.box);
					
					FrameLayout lboxItem2 = (FrameLayout) inflater.inflate(R.layout.quiz_box_item, container,
							false);
					tv = (TextView) lboxItem2.findViewById(R.id.quiz_item);
					tv.setText(""+strokeTotal);
					
					lboxItem2.setOnTouchListener(new MyOnTouch());
					lboxItem2.setId(strokeTotal);
					
					lbox2.addView(lboxItem2);
					
					lbox2.setOnDragListener(new MyOnDrag(inflater,container));
					
					
					if (addAt!=-1) lbot.addView(lboxContainer2,addAt);
					else lbot.addView(lboxContainer2);
				}
				//bot, make this random, probably move on to another loop after this
				
			}
			bttNext.setOnClickListener(new MyOnClick(correctAns));
			
			verticalPager.addView(ll);
			
		}
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mCallback = (FragmentListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement FragmentListener");
		}
	}
	


}
