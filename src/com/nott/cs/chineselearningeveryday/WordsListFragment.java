package com.nott.cs.chineselearningeveryday;

import java.util.ArrayList;

import com.nott.cs.chineselearningeveryday.model.WordsClass;
import com.nott.cs.chineselearningeveryday.util.DataSource;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;



public class WordsListFragment extends Fragment {
	
	FragmentListener mCallback;
	DataSource datasource;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.words_list_fragment, container, false);
		datasource = new DataSource(getActivity());
		datasource.open();
		
		ItemAdapter adapter = new ItemAdapter(getActivity(), R.layout.words_list_item, (ArrayList<WordsClass>) datasource.getAllWords());
		
		ListView listWords = (ListView) rootView.findViewById(R.id.list_words);
		listWords.setAdapter(adapter);
		
		listWords.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position,
					long id) {
				mCallback.showItem(4, position+1, 0);
				// TODO Auto-generated method stub
				
			}
		});
		
		datasource.close();
		return rootView;
	}
	
	// ----initialise mCallback to be able to communicate with Main activity----
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mCallback = (FragmentListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement FragmentListener");
		}
	}

	
	public class ItemAdapter extends ArrayAdapter<WordsClass> {

		// declaring our ArrayList of items
		private ArrayList<WordsClass> objects;

		/* here we must override the constructor for ArrayAdapter
		* the only variable we care about now is ArrayList<Item> objects,
		* because it is the list of objects we want to display.
		*/
		public ItemAdapter(Context context, int textViewResourceId, ArrayList<WordsClass> objects) {
			super(context, textViewResourceId, objects);
			this.objects = objects;
		}

		/*
		 * we are overriding the getView method here - this is what defines how each
		 * list item will look.
		 */
		public View getView(int position, View convertView, ViewGroup parent){

			// assign the view we are converting to a local variable
			View v = convertView;
			
			// first check to see if the view is null. if so, we have to inflate it.
			// to inflate it basically means to render, or show, the view.
			if (v == null) {
				LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = inflater.inflate(R.layout.words_list_item, null);
			}

			/*
			 * Recall that the variable position is sent in as an argument to this method.
			 * The variable simply refers to the position of the current object in the list. (The ArrayAdapter
			 * iterates through the list we sent it)
			 * 
			 * Therefore, i refers to the current Item object.
			 */
			WordsClass word = objects.get(position);

			if (word != null) {

				// This is how you obtain a reference to the TextViews.
				// These TextViews are created in the XML files we defined.

				TextView tchi = (TextView) v.findViewById(R.id.chi_word);
				TextView teng = (TextView) v.findViewById(R.id.eng_word);
				
				tchi.setText(word.getWord());
				teng.setText(word.getEng());
				
				ImageView writeAvail = (ImageView) v.findViewById(R.id.write_avail);
				datasource.open();
				if (datasource.getAllStrokeWriteInfoWithWordID(word.getWordID()).isEmpty())
					writeAvail.setVisibility(View.GONE);
				else writeAvail.setVisibility(View.VISIBLE);
				// check to see if each individual textview is null.
				// if not, assign some text!
				datasource.close();
			}
			
			v.setId(word.getWordID());
			// the view must be returned to our activity
			return v;

		}

	}
}
