package com.nott.cs.chineselearningeveryday;

/**
 * @author Vm Tung txv03u
 */

import java.util.ArrayList;
import java.util.List;

import com.nott.cs.chineselearningeveryday.model.LastAccessClass;
import com.nott.cs.chineselearningeveryday.model.PhraseItemClass;
import com.nott.cs.chineselearningeveryday.model.ScoreClass;
import com.nott.cs.chineselearningeveryday.model.SubMenuClass;
import com.nott.cs.chineselearningeveryday.util.DataSource;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableLayout;

public class RightMenuFragment extends Fragment {
	private DataSource datasource;

	public static String CONTENT_ID = "contentID";
	public static int MAIN_MENU = 0;
	public static int SUB_MENU_SCENARIO = 1;
	public static int SUB_MENU_PHRASES = 2;
	public static int SUB_MENU_QUIZZES = 3;
	public static int SUB_MENU_BLANK = 4;

	int contentID = 0;
	FragmentListener mCallback;
	LayoutInflater inflater;
	ViewGroup container;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		this.inflater = inflater;
		this.container = container;

		LinearLayout rootView = (LinearLayout) inflater.inflate(R.layout.right_fragment, container, false);

		contentID = getArguments().getInt(CONTENT_ID);

		datasource = new DataSource(getActivity());
		datasource.open();
		// datasource.setLastAccess(1);
		if (contentID == MAIN_MENU) {

			rootView.addView(addLastAccessedList());
			rootView.addView(addSeparator());
			rootView.addView(addFavouritePhrasesList());
			rootView.addView(addSeparator());
			rootView.addView(addScoresList());

		} else if (contentID == SUB_MENU_SCENARIO) {
			rootView.addView(addLastAccessedList());

		} else if (contentID == SUB_MENU_PHRASES) {
			rootView.addView(addFavouritePhrasesList());

		} else if (contentID == SUB_MENU_QUIZZES) {
			rootView.addView(addScoresList());

		} else if (contentID == SUB_MENU_BLANK ) {
			
			
		}

		datasource.close();
		return rootView;

	}

	private View addSeparator() {
		View separator = inflater.inflate(R.layout.right_separator, container, false);
		return separator;
	}

	private LinearLayout addLastAccessedList() {
		LinearLayout rightMenuItemView = (LinearLayout) inflater.inflate(R.layout.right_item_last_accessed, container,
				false);
		rightMenuItemView.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT, 1f));

		List<LastAccessClass> allLastAccessClass = datasource.getAllLastAccess();
		List<String> allLastAccessItem = new ArrayList<String>();
		for (int i = 0; i < allLastAccessClass.size(); i++) {
			allLastAccessItem.add(datasource.getScenarioTypesWithID(allLastAccessClass.get(i).getScenarioTypeID())
					.getName());
		}

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.drawer_list_item,
				allLastAccessItem);

		ListView listLastAccessed = (ListView) rightMenuItemView.findViewById(R.id.listLastAccessed);
		listLastAccessed.setAdapter(adapter);
		listLastAccessed.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
				datasource.open();
				mCallback.showItem(2, datasource.getAllLastAccess().get(position).getScenarioTypeID(), 0);
				// TODO Auto-generated method stub
				datasource.close();

			}
		});
		return rightMenuItemView;
	}

	private LinearLayout addFavouritePhrasesList() {
		LinearLayout rightMenuItemView = (LinearLayout) inflater.inflate(R.layout.right_item_favourite_phrases,
				container, false);
		rightMenuItemView.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT, 1f));

		List<PhraseItemClass> allFavouritePhrases = datasource.getAllFavouritePhrase();
		List<String> allFavouritePhrasesItem = new ArrayList<String>();
		for (int i = 0; i < allFavouritePhrases.size(); i++) {
			allFavouritePhrasesItem.add(allFavouritePhrases.get(i).getPhrase());
			Log.w("test", "not null");
		}

		ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getActivity(), R.layout.drawer_list_item,
				allFavouritePhrasesItem);

		ListView listFavourite = (ListView) rightMenuItemView.findViewById(R.id.listFavourite);

		listFavourite.setAdapter(adapter2);
		listFavourite.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
				datasource.open();
				List<PhraseItemClass> allFavouritePhrases = datasource.getAllFavouritePhrase();
				mCallback.showItem(3, allFavouritePhrases.get(position).getTypeID(), allFavouritePhrases.get(position)
						.getPhraseID());
				// TODO Auto-generated method stub
				datasource.close();
			}
		});

		return rightMenuItemView;
	}

	private LinearLayout addScoresList() {
		LinearLayout rightMenuItemView = (LinearLayout) inflater.inflate(R.layout.right_item_scores, container, false);
		rightMenuItemView.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT, 1f));

		List<SubMenuClass> allSubMenuClass = datasource.getAllQuizTypes();
		List<String> allAverageScores = new ArrayList<String>();
		for (int i = 0; i < allSubMenuClass.size(); i++) {
			List<ScoreClass> allScoreOfAType = datasource.getAllScoreWithTypeID(allSubMenuClass.get(i).getId());
			float total = 0;
			for (int j = 0; j < allScoreOfAType.size(); j++) {
				total += allScoreOfAType.get(j).getScore();
			}

			allAverageScores.add(allSubMenuClass.get(i).getName() + ": " + total * 1.0 / allScoreOfAType.size());

		}

		ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(getActivity(), R.layout.drawer_list_item,
				allAverageScores);

		ListView listScores = (ListView) rightMenuItemView.findViewById(R.id.listScores);
		listScores.setAdapter(adapter3);
		listScores.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
				datasource.open();
				mCallback.showItem(5, datasource.getAllQuizTypes().get(position).getId(), -1);
				// TODO Auto-generated method stub
				datasource.close();
			}
		});

		return rightMenuItemView;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mCallback = (FragmentListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement FragmentListener");
		}
	}

}
