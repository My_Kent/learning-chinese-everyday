package com.nott.cs.chineselearningeveryday;

import java.util.List;

import com.nott.cs.chineselearningeveryday.model.ConversationLineClass;
import com.nott.cs.chineselearningeveryday.model.IntroClass;
import com.nott.cs.chineselearningeveryday.util.DataSource;
import com.nott.cs.chineselearningeveryday.util.MPlayer;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class ItemFragment extends Fragment {
	FragmentListener mCallback;

	DataSource datasource;
	int item_type;
	int parent_type_id;
	int currentLineID=1;
	
	RelativeLayout rootView;
	
	LinearLayout speechBubble;
	
	List<ConversationLineClass> conversationLineList;
	List<IntroClass> introList;
	
	TextView chiLine;
	TextView pinyinLine;
	TextView engLine;
	TextView Name;
	
	TextView progress;
	
	Button bttBack;
	Button bttNext;
	Button bttBack5;
	Button bttNext5;
	
	ImageView background;
	
	String currentBackground="";
	
	public static final String ITEM_TYPE="item_type";
	public static final String PARENT_TYPE_ID="parent_type_id";
	
	int mShortAnimationDuration;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		mShortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);
		
		datasource = new DataSource(getActivity());
		datasource.open();
		
		item_type = getArguments().getInt(ITEM_TYPE);
		parent_type_id = getArguments().getInt(PARENT_TYPE_ID);

		
		rootView = (RelativeLayout) inflater.inflate(R.layout.scene_item, container,
				false);
		
		speechBubble = (LinearLayout) rootView.findViewById(R.id.bubble);
		
		Name = (TextView) rootView.findViewById(R.id.Name);
		
		chiLine = (TextView) rootView.findViewById(R.id.chiLine);
		pinyinLine = (TextView) rootView.findViewById(R.id.pinyinLine);
		engLine = (TextView) rootView.findViewById(R.id.engLine);
		
		bttBack = (Button) rootView.findViewById(R.id.bttBack);
		bttNext = (Button) rootView.findViewById(R.id.bttNext);
		bttBack5 = (Button) rootView.findViewById(R.id.bttBack5);
		bttNext5 = (Button) rootView.findViewById(R.id.bttNext5);
		
		background = (ImageView) rootView.findViewById(R.id.background);
		
		progress = (TextView) rootView.findViewById(R.id.progress);
		
		if (item_type==1) {
			introList = datasource.getAllIntroClass();
			
			showIntro(currentLineID);
			
			bttBack.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					showIntro(--currentLineID);
					
				}
			});
			
			bttNext.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					showIntro(++currentLineID);
				}
			});

			
			bttBack5.setVisibility(View.GONE);
			bttNext5.setVisibility(View.GONE);
			
			speechBubble.setVisibility(View.GONE);
			
		} else if (item_type==2) {
			conversationLineList = datasource.getAllConversationLineWithTypeID(parent_type_id);
			datasource.setLastAccess(parent_type_id);
			
			showScene(currentLineID);
			
			bttBack.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					showScene(--currentLineID);
				}
			});
			bttNext.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					showScene(++currentLineID);
				}
			});
			bttBack5.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					currentLineID-=5;
					if (currentLineID<1) currentLineID=1;
					showScene(currentLineID);
				}
			});
			bttNext5.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					currentLineID+=5;
					if (currentLineID>conversationLineList.size()) currentLineID=conversationLineList.size();
					showScene(currentLineID);
				}
			});
			
			mCallback.showRightMenu(RightMenuFragment.SUB_MENU_SCENARIO);
		}
		

		//change to linear layout
		
		datasource.close();
		return rootView;
	}
	
	ConversationLineClass currentLine;
	public void showScene(int id){
		
		progress.setText(id+"/"+conversationLineList.size());
		
		currentLine =conversationLineList.get(id-1); 
		String mDrawableName = currentLine.getImg();
		if (!currentBackground.equals(mDrawableName)) {
			currentBackground=mDrawableName;
			
			
			background.animate()
            .alpha(0f)
            .setDuration(2*mShortAnimationDuration)
            .setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                	int resID = getResources().getIdentifier(currentBackground , "drawable", getActivity().getPackageName());
                	background.setImageResource(resID);
                	background.animate()
                    .alpha(1f)
                    .setDuration(2*mShortAnimationDuration)
                    .setListener(null);

                }
            });

		}
		//
    	//get id of the image from the name
        
		speechBubble.animate()
        .alpha(0f)
        .setDuration(2*mShortAnimationDuration)
        .setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
            	Name.setText(currentLine.getSpeaker()+": ");
                chiLine.setText(currentLine.getChi());
                pinyinLine.setText(currentLine.getPinyin());
                engLine.setText(currentLine.getEng());
                
                if (currentLine.getOrderID()==0) speechBubble.setBackgroundResource(getResources().getIdentifier("speech_bubble_left" , "drawable", getActivity().getPackageName()));
        		else if (currentLine.getOrderID()==1) speechBubble.setBackgroundResource(getResources().getIdentifier("speech_bubble_right" , "drawable", getActivity().getPackageName()));
        		else speechBubble.setBackgroundResource(getResources().getIdentifier("speech_bubble" , "drawable", getActivity().getPackageName()));
        		                
            	speechBubble.animate()
                .alpha(1f)
                .setDuration(2*mShortAnimationDuration).setListener(null);

            }
        });

		        
        bttBack.setEnabled(true);
   	 	bttNext.setEnabled(true);
   	 	bttBack5.setEnabled(true);
	 	bttNext5.setEnabled(true);
        if (id==1) {
        	bttBack.setEnabled(false);
        	bttBack5.setEnabled(false);
        }
        if (id==conversationLineList.size()) {
        	bttNext.setEnabled(false);
        	bttNext5.setEnabled(false);
        }
        
        speechBubble.setTag(currentLine.getAudio());
		speechBubble.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				MPlayer.playSound(getActivity(), getResources().getIdentifier((String) v.getTag(), "raw", getActivity().getPackageName()));
			}
		});
		
		
	}
	
	
	public void showIntro(int id){
		
		progress.setText(id+"/"+introList.size());
		
		IntroClass currentIntro = introList.get(id-1); 
		String mDrawableName = currentIntro.getImg();
		if (!currentBackground.equals(mDrawableName)) {
			currentBackground=mDrawableName;
			
			
			background.animate()
            .alpha(0f)
            .setDuration(2*mShortAnimationDuration)
            .setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                	int resID = getResources().getIdentifier(currentBackground , "drawable", getActivity().getPackageName());
                	background.setImageResource(resID);
                	background.animate()
                    .alpha(1f)
                    .setDuration(2*mShortAnimationDuration)
                    .setListener(null);

                }
            });

		}
		
        bttBack.setEnabled(true);
   	 	bttNext.setEnabled(true);

   	 	if (id==1) {
        	bttBack.setEnabled(false);
        	
        }
        if (id==introList.size()) {
        	bttNext.setEnabled(false);
        	
        }
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mCallback = (FragmentListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement FragmentListener");
		}
	}
}
