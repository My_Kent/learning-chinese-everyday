package com.nott.cs.chineselearningeveryday;

/**
 * @author Vm Tung txv03u
 */

import java.util.List;
import java.util.Random;

import com.nott.cs.chineselearningeveryday.model.ScoreClass;
import com.nott.cs.chineselearningeveryday.model.StrokeWriteInfoClass;
import com.nott.cs.chineselearningeveryday.model.WordsClass;
import com.nott.cs.chineselearningeveryday.util.DataSource;
import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class WriteFragment extends Fragment {
	FragmentListener mCallback;
	private DrawingView drawView;
	private DrawingView learnView;
	private Button bttClear, bttMark, bttNext;
	private DataSource datasource;
	public static String LEARN = "learn";
	int wordID;
	boolean learn = false;

	int currentLearn = 0;

	Button bttBackStroke;
	Button bttNextStroke;

	Handler handler = new Handler();
	float section = 0;

	public WriteFragment(){}
	
	List<StrokeWriteInfoClass> strokeWriteInfoList;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		datasource = new DataSource(getActivity());
		datasource.open();
		
		
		
		learn = getArguments().getBoolean(LEARN, false);
		View rootView;
		rootView = inflater.inflate(R.layout.activity_draw_learn, container, false);

		if (learn) {
			
			wordID=getArguments().getInt(VerticalFragment.PARENT_TYPE_ID);
			strokeWriteInfoList = datasource.getAllStrokeWriteInfoWithWordID(wordID);
		} else{
			//rootView = inflater.inflate(R.layout.activity_draw, container, false);
	
			Random r = new Random();
			int i = r.nextInt(datasource.getAllStrokeWriteInfo().size());
			wordID = datasource.getAllStrokeWriteInfo().get(i).getWordID();
		}
		
		
		WordsClass currentWord = datasource.getWordsWithID(wordID);
		
		
		
		TextView chiWord = (TextView) rootView.findViewById(R.id.chiWord);
		chiWord.setText(currentWord.getWord());
		TextView pinyinWord = (TextView) rootView.findViewById(R.id.pinyinWord);
		pinyinWord.setText(currentWord.getPinyin());
		TextView engWord = (TextView) rootView.findViewById(R.id.engWord);
		engWord.setText(currentWord.getEng());
		
		TextView strokeNum = (TextView) rootView.findViewById(R.id.strokeNum);
		
		
		TextView writeAvail = (TextView) rootView.findViewById(R.id.write_avail);
		
		if (learn) {
			strokeNum.setText("Number of strokes: "+currentWord.getStrokeNum());
			LinearLayout learnLayout = (LinearLayout) rootView.findViewById(R.id.writing);
			if (datasource.getAllStrokeWriteInfoWithWordID(wordID).isEmpty()) {
				writeAvail.setText("Writing is not available");
				learnLayout.setVisibility(View.GONE);
			}
			
		} else {
			strokeNum.setVisibility(View.GONE);
			writeAvail.setText("Write the word below");
		}
		
		drawView = (DrawingView) rootView.findViewById(R.id.drawing);
		bttClear = (Button) rootView.findViewById(R.id.new_btn);
		bttClear.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				drawView.clearCanvas();
			}
		});
		bttMark = (Button) rootView.findViewById(R.id.mark_btn);
		bttMark.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				datasource.open();
				drawView.checkStrokeAndUpadteScore(datasource.getAllVectorStrokeWriteInfoWithWordID(wordID));
				
				if (!learn) {
					bttClear.setEnabled(false);
					bttMark.setEnabled(false);
					drawView.setDisableDraw(true);
					datasource.setScore(new ScoreClass(2,drawView.getScore()));
				}
				
				
				final Dialog scoreDialog = new Dialog(getActivity());
				scoreDialog.setTitle("Your score:");
				scoreDialog.setContentView(R.layout.score_dialog);
				TextView score = (TextView) scoreDialog.findViewById(R.id.score);
				score.setText("Score: " + drawView.getScore());
				score.setTextSize(25);
				Button bttOK = (Button) scoreDialog.findViewById(R.id.OK_btt);
				bttOK.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {

						scoreDialog.dismiss();
					}
				});
				
				datasource.close();
				scoreDialog.show();
				
			}
		});
		
		bttNext = (Button) rootView.findViewById(R.id.nextQuiz);
		learnView = (DrawingView) rootView.findViewById(R.id.drawing_learn);
		bttBackStroke = (Button) rootView.findViewById(R.id.btt_draw_learn_back);
		bttNextStroke = (Button) rootView.findViewById(R.id.btt_draw_learn_next);
		if (!learn) {
			
		
			// temporary
			bttNext.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mCallback.showItem(5, 3, 0);
				}
			});
			
			learnView.setVisibility(View.GONE);
			bttBackStroke.setVisibility(View.GONE);
			bttNextStroke.setVisibility(View.GONE);
			

			TextView tv = (TextView) rootView.findViewById(R.id.practice);
			tv.setVisibility(View.GONE);
			tv = (TextView) rootView.findViewById(R.id.sample);
			tv.setVisibility(View.GONE);
			
		}
		// ---init learn view--

		else if (learn) {
			bttNext.setVisibility(View.GONE);
			
			learnView.setDisableDraw(true);



			// back.setEnabled(false);
			bttBackStroke.setId(0);
			bttNextStroke.setId(1);
			
			bttBackStroke.setEnabled(false);

			bttBackStroke.setOnClickListener(new onClickLearnListener());
			bttNextStroke.setOnClickListener(new onClickLearnListener());

		}
		System.out.println("write");
		return rootView;
	}

	public class onClickLearnListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			
			// TODO Auto-generated method stub
			handler.removeCallbacks(runnable);
			if (v.getId() == 1)
				currentLearn++;
			else
				currentLearn--;
			int maxsize = strokeWriteInfoList.get(strokeWriteInfoList.size()-1).getStrokeOrder();
			if (currentLearn == 0)
				bttBackStroke.setEnabled(false);
			else if (currentLearn == maxsize)
				bttNextStroke.setEnabled(false);
			else {
				bttBackStroke.setEnabled(true);
				bttNextStroke.setEnabled(true);
			}

			
			drawLearn();
		}

	}

	Runnable runnable = new Runnable() {
		@Override
		public void run() {
			datasource.open();
			/* do what you need to do */
			section += 0.1;
			
			learnView.drawLearn(datasource.getAllVectorStrokeWriteInfoWithWordID(wordID), start+currStrokeDraw, section);
			datasource.close();
			/* and here comes the "trick" */
			if (section < 1)
				handler.postDelayed(this, 50);
			else if (currStrokeDraw+1<strokeNum) {
				section = (float) -0.1;
				currStrokeDraw++;
				handler.postDelayed(this, 50);
			}
			
		}
	};

	int strokeNum=1,start=-1, currStrokeDraw=0;
	public void drawLearn() {
		section = (float) -0.1;
		int count=0;
		strokeNum=1;
		currStrokeDraw=0;
		start=-1;
		for (int i = 0; i<strokeWriteInfoList.size() && strokeWriteInfoList.get(i).getStrokeOrder() <= currentLearn ; i++) {
			if (strokeWriteInfoList.get(i).getStrokeOrder() == currentLearn) {
				count++;
				if (start==-1) start=i+1;
			}
			
		}
		

		if (count>=2)
			strokeNum=count;
		
		runnable.run();
	}

	// ----initialise mCallback to be able to communicate with Main activity----
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mCallback = (FragmentListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement FragmentListener");
		}
	}
}
