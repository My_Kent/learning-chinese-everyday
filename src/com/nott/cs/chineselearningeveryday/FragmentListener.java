package com.nott.cs.chineselearningeveryday;

/**
 * @author Vm Tung txv03u
 */

import java.util.List;

//----this interface will be used to send info to main activity from fragment----
//example of usage in menufragment.java, mainactivity.java must also implement this.

public interface FragmentListener {
	public void onMenuItemSelected(int position);
	public void showItem(int item_type, int parent_type_id, int special_request);
	public void showRightMenu(int contentID);
	public void lockRightDrawer(boolean lock);
}
