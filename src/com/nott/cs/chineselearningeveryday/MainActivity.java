package com.nott.cs.chineselearningeveryday;


// THIS CLASS NEEDS SORTING!
// ALL UNUSED CODE NEEDS DELETING :) mxh22u

/**
 * @author Vm Tung txv03u
 * Refactored by Martin Handley mxh22u
 */

import android.os.Bundle;
import android.content.res.Configuration;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;

public class MainActivity extends FragmentActivity implements FragmentListener {

	private static final String MENU_TAG = "menu";
	private static final String SUB_MENU_TAG = "submenu";
	private static final String ITEM = "item";
	private static final int MENU_MIN_POSITION = 0; // lower and upper bounds of
													// menu options
	private static final int MENU_MAX_POSITION = 6;

	private DrawerLayout mDrawerLayout; // Main layout of the activity
	private ListView mLeftNavList; // Left navigation menu
	private FrameLayout mRightNavList; // Right navigation menu; currently left
										// blank
	private ActionBarDrawerToggle mDrawerToggle;
	private CharSequence mDrawerTitle;
	private CharSequence mTitle;
	private String[] mOptionTitles;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		// Set the main activity view
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Acquire resources
		mTitle = mDrawerTitle = getActionBar().getTitle(); // Activity title

		// Main layout of activity
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		// Left navigation menu
		mLeftNavList = (ListView) findViewById(R.id.left_drawer); 
		// Right navigation menu
		mRightNavList = (FrameLayout) findViewById(R.id.right_drawer); 
		// Option headings
		mOptionTitles = getResources().getStringArray(R.array.left_options_array); 
		// Set up the left navigation menu with option headings
		mLeftNavList.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, mOptionTitles));
		// Set listener for the left drawer
		mLeftNavList.setOnItemClickListener(new LeftNavClickListener());

		mDrawerToggle = new ActionBarDrawerToggle(this, // Host activity
				mDrawerLayout, // DrawerLayout object
				R.drawable.ic_drawer, // Nav drawer image to replace 'up' caret
				R.string.drawer_open, R.string.drawer_close) {

			public void onDrawerClosed(View view) {
				//getActionBar().setTitle(mTitle);
				invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			}

			public void onDrawerOpened(View drawerView) {
				//getActionBar().setTitle(mDrawerTitle);
				invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			}
		};

		// enable ActionBar app icon to behave as action to toggle nav drawer
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		showRightMenu(RightMenuFragment.MAIN_MENU);

		if (savedInstanceState == null) {

			getActionBar().setTitle("Chinese Learning Every Day");
			onMenuItemSelected(-1);

		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Pass the event to ActionBarDrawerToggle, if it returns
		// true, then it has handled the app icon touch event
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		// Handle your other action bar items...
		return super.onOptionsItemSelected(item);
	}

	private class LeftNavClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			onMenuItemSelected(position);
		}

	}


	// ------- Fragment Listener ----------

	public void onMenuItemSelected(int position) {
		// update the main content by replacing fragments

		// mark the menu item as selected
		mLeftNavList.setItemChecked(position, true);

		String fragment_tag = null;
		Fragment fragment;

		

		int flag = 0;
		Bundle args = new Bundle();
		if (position > MENU_MIN_POSITION && position < MENU_MAX_POSITION) {
			if (position==4) {
				fragment= new WordsListFragment();
				
			}
			else {
				fragment = new ParallaxFragment();
	
				// args.putInt(ParallaxFragment.SCROLL_POS, scroll);
				args.putInt(ParallaxFragment.VIEW_TYPE, position);
			}
				fragment_tag = SUB_MENU_TAG;
			
		} else {
			if (position < 0)
				flag = 1;
			position = 0;
			fragment = new MenuFragment();
			fragment_tag = MENU_TAG;
		}

		fragment.setArguments(args);
		FragmentManager fragmentManager = getSupportFragmentManager();
		fragmentManager.popBackStack("menu", FragmentManager.POP_BACK_STACK_INCLUSIVE);
		if (flag == 1)
			fragmentManager.beginTransaction().replace(R.id.content_frame, fragment, fragment_tag).commit();
		else
			fragmentManager.beginTransaction().replace(R.id.content_frame, fragment, fragment_tag)
					.addToBackStack("menu").commit();
		// adds to fragment to the stack so that user's can use their 'back'
		// button to return to the previous menu
		// good job Tung!

		// .addToBackStack(null)
		// update selected item and title, then close the drawer
		mLeftNavList.setItemChecked(position, true);
		
		if (position==1 || position==4)
			mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, mRightNavList);
		else mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, mRightNavList);
		
		mDrawerLayout.closeDrawer(mLeftNavList);
		mDrawerLayout.closeDrawer(mRightNavList);
		// close left and right sliding menus
	}

	public void showItem(int item_type, int parent_type_id, int special_request) {
		// update the main content by replacing fragments
		Fragment fragment = null;
		Bundle args = new Bundle();

		if (item_type==4) {
			fragment = new WriteFragment();
			args.putInt(VerticalFragment.PARENT_TYPE_ID, parent_type_id);
			args.putBoolean(WriteFragment.LEARN, true);
		} else if (item_type == 5 && parent_type_id == 3) {
			fragment = new WriteFragment();
			args.putInt(VerticalFragment.PARENT_TYPE_ID, parent_type_id);
		} else {
			if (item_type == 2)
				fragment = new ItemFragment();
			else if (item_type==1 && special_request==-1){
				if (parent_type_id==1) fragment=new ItemFragment();
				else fragment = new SubSubMenuFragment();
			} else fragment = new VerticalFragment();
			args.putInt(VerticalFragment.ITEM_TYPE, item_type);
			args.putInt(VerticalFragment.PARENT_TYPE_ID, parent_type_id);
			args.putInt(VerticalFragment.SPECIAL_REQUEST, special_request);

		}
		String fragment_tag = ITEM;

		fragment.setArguments(args);
		FragmentManager fragmentManager = getSupportFragmentManager();
		fragmentManager.popBackStack("sub", FragmentManager.POP_BACK_STACK_INCLUSIVE);
		fragmentManager.beginTransaction().replace(R.id.content_frame, fragment, fragment_tag).addToBackStack("sub")
				.commit();
		
		mDrawerLayout.closeDrawer(mLeftNavList);
		mDrawerLayout.closeDrawer(mRightNavList);
	}

	public void showRightMenu(int contentID) {
		Fragment fragment = null;
		Bundle args = new Bundle();

		fragment = new RightMenuFragment();

		args.putInt(RightMenuFragment.CONTENT_ID, contentID);

		fragment.setArguments(args);
		FragmentManager fragmentManager = getSupportFragmentManager();
		fragmentManager.beginTransaction().replace(R.id.right_drawer, fragment, null).commit();
		
		mDrawerLayout.closeDrawer(mLeftNavList);
		mDrawerLayout.closeDrawer(mRightNavList);
	}
	
	public void lockRightDrawer(boolean lock){
		if (lock) mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, mRightNavList);
		else mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, mRightNavList);
	}


}
