package com.nott.cs.chineselearningeveryday;

/**
 * @author Vm Tung txv03u
 */

import java.util.ArrayList;
import java.util.List;
import com.nott.cs.chineselearningeveryday.model.VectorClass;
import android.view.View;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.view.MotionEvent;
import android.util.TypedValue;

public class DrawingView extends View {

	// drawing path
	private Path drawPath;
	// drawing and canvas paint
	private Paint drawPaint, canvasPaint;
	// initial color
	private int paintColor = 0xFF000000;
	// canvas
	private Canvas drawCanvas;
	// canvas bitmap
	private Bitmap canvasBitmap;
	private float brushSize, lastBrushSize;

	float upX = 0, upY = -1;
	float startX = 0, startY = 0, lastX = 0, lastY = 0;
	float currVectX = 0, currVectY = 0;
	float offlim = (float) (Math.PI / 18);
	int notSraight = 0;
	float stray = 0;
	float lineAlpha = 0;
	boolean straightLine = false;
	float totalHighScore = 0;
	List<VectorClass> lineDrawnList = new ArrayList<VectorClass>();

	boolean disable = false;

	public void clearCanvas() {
		startX = 0;
		startY = 0;
		lastX = 0;
		lastY = 0;
		currVectX = 0;
		currVectY = 0;

		notSraight = 0;
		stray = 0;
		lineAlpha = 0;
		straightLine = false;
		totalHighScore = 0;
		lineDrawnList = new ArrayList<VectorClass>();

		drawCanvas.drawColor(Color.WHITE);
		invalidate();
	}

	public void setDisableDraw(boolean disable) {
		this.disable = disable;
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		canvasBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
		drawCanvas = new Canvas(canvasBitmap);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		canvas.drawBitmap(canvasBitmap, 0, 0, canvasPaint);
		canvas.drawPath(drawPath, drawPaint);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (disable) {
			Log.w("x", event.getX() + "");
			Log.w("y", event.getY() + "");

			return false;
		}
		float touchX = event.getX();
		float touchY = event.getY();
		// important: start point, end point (further get lower mark), angle,
		// stray is for mark down
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			stray = 0;
			lastX = touchX;
			startX = touchX;
			lastY = touchY;
			startY = touchY;

			drawPath.moveTo(touchX, touchY);
			break;
		case MotionEvent.ACTION_MOVE:
			drawPath.lineTo(touchX, touchY);
			currVectX = touchX - lastX;
			currVectY = touchY - lastY;
			float cosine = (float) ((currVectX * upX + currVectY * upY) / (Math.sqrt(currVectX * currVectX + currVectY
					* currVectY) * Math.sqrt(upX * upX + upY * upY)));
			if (cosine > 1)
				cosine = 1;
			float currentAlpha = 0;
			if (straightLine == false) {
				lineAlpha = (float) Math.acos(cosine);
			}
			currentAlpha = Math.abs((float) Math.acos(cosine) - lineAlpha);

			if (currentAlpha < offlim) {
				// recheck
				if (touchX > lastX)
					stray += currentAlpha;
				else
					stray -= currentAlpha;

				notSraight = 0;
				if (straightLine == false) {
					startX = lastX;
					startY = lastY;
				}
				straightLine = true;
				if (stray < -4 * offlim || stray > 4 * offlim) {
					straightLine = false;
				}

			} else {
				if (notSraight == 5) {
					stray = 0;
					straightLine = false;
					lineDrawnList.add(new VectorClass(startX, startY, touchX, touchY));
					
				} else if (straightLine == true) {
					notSraight++;
				}
			}

			lastX = touchX;
			lastY = touchY;
			
			break;
		case MotionEvent.ACTION_UP:
			lineDrawnList.add(new VectorClass(startX, startY, touchX, touchY));
			drawCanvas.drawPath(drawPath, drawPaint);
			drawPath.reset();
			
			break;
		default:
			return false;
		}
		invalidate();

		return true;
	}

	public float getAlpha(float x1, float y1, float x2, float y2) {
		float cosine = (float) ((x1 * x2 + y1 * y2) / (Math.sqrt(x1 * x1 + y1 * y1) * Math.sqrt(x2 * x2 + y2 * y2)));
		if (cosine > 1)
			cosine = 1;
		return (float) Math.acos(cosine);
	}

	public float getScore() {
		return totalHighScore;
	}

	public boolean checkStrokeAndUpadteScore(List<VectorClass> strokeSet) {
		List<VectorClass> finalAcceptedList = new ArrayList<VectorClass>();
		float totalTempScore = 0;
		float scoreUnit = 0;
		scoreUnit = (float) (10.0 / (strokeSet.size()));
		// rect.add(new vectorClass((float) 0.5,0,(float) 0.5,1));
		// rect.add(new vectorClass(1,1,0,1));
		// rect.add(new vectorClass(0,1,0,0));

		// check alpha of each pair
		// similar then pair
		// calculate ratio
		// compare all the other end points

		for (int i = 0; i < lineDrawnList.size(); i++) {
			for (int j = 0; j < strokeSet.size(); j++) {
				VectorClass currentDrawnLine = lineDrawnList.get(i);
				VectorClass currentStrokeModel = strokeSet.get(j);
				// recheck alpha
				int totalAccepted = 0;
				List<VectorClass> tempAcceptedList = new ArrayList<VectorClass>();
				if (getAlpha(currentDrawnLine.getVectX(), currentDrawnLine.getVectY(), currentStrokeModel.getVectX(),
						currentStrokeModel.getVectY()) < offlim) {

					totalAccepted++;

					List<Integer> already = new ArrayList<Integer>();
					already.add(i);

					totalTempScore = 0;
					if (j == 0)
						totalTempScore += scoreUnit;
					else
						totalTempScore += scoreUnit / 3;

					float offsetX = currentDrawnLine.getStartX() - currentStrokeModel.getStartX();
					float offsetY = currentDrawnLine.getStartY() - currentStrokeModel.getStartY();
					float ratio = currentDrawnLine.getLength() / currentStrokeModel.getLength();

					float newStartX1 = (float) (offsetX);
					float newStartY1 = (float) (offsetY);
					float newEndX1 = (float) (offsetX + ratio
							* (currentStrokeModel.getEndX() - currentStrokeModel.getStartX()));
					float newEndY1 = (float) (offsetY + ratio
							* (currentStrokeModel.getEndY() - currentStrokeModel.getStartY()));

					tempAcceptedList.add(new VectorClass(newStartX1, newStartY1, newEndX1, newEndY1));

					int lastAcceptedStroke = i;
					for (int k = 0; k < strokeSet.size(); k++) {
						if (k != j) {

							float newStartX = (float) (offsetX + ratio
									* (strokeSet.get(k).getStartX() - currentStrokeModel.getStartX()));
							float newStartY = (float) (offsetY + ratio
									* (strokeSet.get(k).getStartY() - currentStrokeModel.getStartY()));
							float newEndX = (float) (offsetX + ratio
									* (strokeSet.get(k).getEndX() - currentStrokeModel.getStartX()));
							float newEndY = (float) (offsetY + ratio
									* (strokeSet.get(k).getEndY() - currentStrokeModel.getStartY()));

							/*
							 * drawPath.moveTo((float)(newStartX-ratio*0.4),
							 * (float)(newStartY-ratio*0.4));
							 * drawPath.lineTo((float)(newStartX+ratio*0.4),
							 * (float)(newStartY-ratio*0.4));
							 * drawPath.lineTo((float)(newStartX+ratio*0.4),
							 * (float)(newStartY+ratio*0.4));
							 * drawPath.lineTo((float)(newStartX-ratio*0.4),
							 * (float)(newStartY+ratio*0.4));
							 * drawPath.lineTo((float)(newStartX-ratio*0.4),
							 * (float)(newStartY-ratio*0.4));
							 * drawCanvas.drawPath(drawPath, drawPaint);
							 * drawPath.reset();
							 * 
							 * drawPath.moveTo((float)(newEndX-ratio*0.4),
							 * (float)(newEndY-ratio*0.4));
							 * drawPath.lineTo((float)(newEndX+ratio*0.4),
							 * (float)(newEndY-ratio*0.4));
							 * drawPath.lineTo((float)(newEndX+ratio*0.4),
							 * (float)(newEndY+ratio*0.4));
							 * drawPath.lineTo((float)(newEndX-ratio*0.4),
							 * (float)(newEndY+ratio*0.4));
							 * drawPath.lineTo((float)(newEndX-ratio*0.4),
							 * (float)(newEndY-ratio*0.4));
							 * drawCanvas.drawPath(drawPath, drawPaint);
							 * drawPath.reset();
							 * 
							 * drawPath.moveTo(newStartX, newStartY);
							 * drawPath.lineTo(newEndX, newEndY);
							 * drawCanvas.drawPath(drawPath, drawPaint);
							 * drawPath.reset();
							 */
							boolean acceptStroke = false;
							float strokeHighScore = 0;
							int acceptedStroke = 0;
							for (int l = 0; l < lineDrawnList.size(); l++) {
								float strokeTempScore = 0;
								if (!already.contains(l)) {
									if (newStartX - ratio * 0.4 < lineDrawnList.get(l).getStartX()
											&& lineDrawnList.get(l).getStartX() < newStartX + ratio * 0.4
											&& newStartY - ratio * 0.4 < lineDrawnList.get(l).getStartY()
											&& lineDrawnList.get(l).getStartY() < newStartY + ratio * 0.4) {
										strokeTempScore += scoreUnit / 3;
										acceptStroke = true;
									} else if (newStartX - ratio * 0.4 < lineDrawnList.get(l).getEndX()
											&& lineDrawnList.get(l).getEndX() < newStartX + ratio * 0.4
											&& newStartY - ratio * 0.4 < lineDrawnList.get(l).getEndY()
											&& lineDrawnList.get(l).getEndY() < newStartY + ratio * 0.4) {
										strokeTempScore += scoreUnit / 6;
										acceptStroke = true;
									}
									if (newEndX - ratio * 0.4 < lineDrawnList.get(l).getEndX()
											&& lineDrawnList.get(l).getEndX() < newEndX + ratio * 0.4
											&& newEndY - ratio * 0.4 < lineDrawnList.get(l).getEndY()
											&& lineDrawnList.get(l).getEndY() < newEndY + ratio * 0.4) {
										strokeTempScore += scoreUnit / 3;
										acceptStroke = true;

									} else if (newStartX - ratio * 0.4 < lineDrawnList.get(l).getStartX()
											&& lineDrawnList.get(l).getStartX() < newStartX + ratio * 0.4
											&& newStartY - ratio * 0.4 < lineDrawnList.get(l).getStartY()
											&& lineDrawnList.get(l).getStartY() < newStartY + ratio * 0.4) {
										strokeTempScore += scoreUnit / 6;
										acceptStroke = true;
									}
								}
								if (strokeHighScore < strokeTempScore) {
									strokeHighScore = strokeTempScore;
									acceptedStroke = l;
									if (strokeHighScore == (scoreUnit / 3) * 2)
										break;
								}

							}
							tempAcceptedList.add(new VectorClass(newStartX, newStartY, newEndX, newEndY));
							if (!already.contains(acceptedStroke)) {
								already.add(acceptedStroke);

							}
							totalTempScore += strokeHighScore;
							if (acceptStroke) {
								if (acceptedStroke > lastAcceptedStroke) {
									totalTempScore += scoreUnit / 3;
								}
								lastAcceptedStroke = acceptedStroke;
								totalAccepted++;

							}
						}
					}

					if (totalTempScore > totalHighScore) {
						totalHighScore = totalTempScore;
						finalAcceptedList = tempAcceptedList;
					}
					if (totalAccepted == strokeSet.size()) {
						drawSolution(finalAcceptedList);
						return true;
					}
				}

			}
		}
		drawSolution(finalAcceptedList);
		return false;

	}

	public void drawSolution(List<VectorClass> finalAcceptedList) {

		for (int i = 0; i < finalAcceptedList.size(); i++) {

			float newStartX = finalAcceptedList.get(i).getStartX();
			float newStartY = finalAcceptedList.get(i).getStartY();
			float newEndX = finalAcceptedList.get(i).getEndX();
			float newEndY = finalAcceptedList.get(i).getEndY();

			/*
			 * drawPath.moveTo((float)(newStartX-ratio*0.4),
			 * (float)(newStartY-ratio*0.4));
			 * drawPath.lineTo((float)(newStartX+ratio*0.4),
			 * (float)(newStartY-ratio*0.4));
			 * drawPath.lineTo((float)(newStartX+ratio*0.4),
			 * (float)(newStartY+ratio*0.4));
			 * drawPath.lineTo((float)(newStartX-ratio*0.4),
			 * (float)(newStartY+ratio*0.4));
			 * drawPath.lineTo((float)(newStartX-ratio*0.4),
			 * (float)(newStartY-ratio*0.4)); drawCanvas.drawPath(drawPath,
			 * drawPaint); drawPath.reset();
			 * 
			 * drawPath.moveTo((float)(newEndX-ratio*0.4),
			 * (float)(newEndY-ratio*0.4));
			 * drawPath.lineTo((float)(newEndX+ratio*0.4),
			 * (float)(newEndY-ratio*0.4));
			 * drawPath.lineTo((float)(newEndX+ratio*0.4),
			 * (float)(newEndY+ratio*0.4));
			 * drawPath.lineTo((float)(newEndX-ratio*0.4),
			 * (float)(newEndY+ratio*0.4));
			 * drawPath.lineTo((float)(newEndX-ratio*0.4),
			 * (float)(newEndY-ratio*0.4)); drawCanvas.drawPath(drawPath,
			 * drawPaint); drawPath.reset();
			 */
			invalidate();
			drawPaint.setColor(0xFFFF0000);
			drawPath.moveTo(newStartX, newStartY);
			drawPath.lineTo(newEndX, newEndY);
			drawCanvas.drawPath(drawPath, drawPaint);
			drawPath.reset();

		}
		invalidate();
		drawPaint.setColor(0xFF000000);

	}

	int currentStrokeNumber = 0;

	public void drawLearn(List<VectorClass> strokeSet, int strokeNumber, float section) {
		
		float offsetX = 220, offsetY = 90;
		float zoom = 60;

		if (currentStrokeNumber != strokeNumber)
			clearCanvas();
		invalidate();
		// drawPaint.setColor(0xFFFF0000);
		int maxsize=strokeSet.size();
		for (int i = 0; i < strokeNumber && i<maxsize; i++) {
			VectorClass currentStroke = strokeSet.get(i);

			float startX = currentStroke.getStartX();
			float startY = currentStroke.getStartY();
			float endX = currentStroke.getEndX();
			float endY = currentStroke.getEndY();

			if (i == strokeNumber - 1) {

				int negativeX = 1, negativeY = 1;
				if (startX > endX)
					negativeX = -1;
				if (startY > endY)
					negativeY = -1;

				endX = startX + negativeX * section * (Math.abs(endX - startX));
				endY = startY + negativeY * section * (Math.abs(endY - startY));

			}
			if (i == strokeNumber - 1 || currentStrokeNumber != strokeNumber) {
				drawPath.moveTo(offsetX + zoom * startX, offsetY + zoom * startY);
				drawPath.lineTo(offsetX + zoom * endX, offsetY + zoom * endY);
				drawCanvas.drawPath(drawPath, drawPaint);
				drawPath.reset();
			}
			invalidate();
			// invalidate();
		}
		if (currentStrokeNumber != strokeNumber)
			currentStrokeNumber = strokeNumber;

	}

	public DrawingView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setupDrawing();
	}

	private void setupDrawing() {
		// get drawing area setup for interaction
		brushSize = 10;
		lastBrushSize = brushSize;
		drawPath = new Path();
		drawPaint = new Paint();
		drawPaint.setColor(paintColor);
		drawPaint.setAntiAlias(true);
		drawPaint.setStrokeWidth(brushSize);
		drawPaint.setStyle(Paint.Style.STROKE);
		drawPaint.setStrokeJoin(Paint.Join.ROUND);
		drawPaint.setStrokeCap(Paint.Cap.ROUND);
		canvasPaint = new Paint(Paint.DITHER_FLAG);
	}

	public void setColor(String newColor) {
		invalidate();
		paintColor = Color.parseColor(newColor);
		drawPaint.setColor(paintColor);
	}

	public void setBrushSize(float newSize) {
		float pixelAmount = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, newSize, getResources()
				.getDisplayMetrics());
		brushSize = pixelAmount;
		drawPaint.setStrokeWidth(brushSize);
	}

	public void setLastBrushSize(float lastSize) {
		lastBrushSize = lastSize;
	}

	public float getLastBrushSize() {
		return lastBrushSize;
	}

}
