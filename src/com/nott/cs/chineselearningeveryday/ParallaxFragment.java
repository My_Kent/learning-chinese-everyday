package com.nott.cs.chineselearningeveryday;

/**
 * @author Vm Tung txv03u
 */

import java.util.List;
import com.nott.cs.chineselearningeveryday.model.SubMenuClass;
import com.nott.cs.chineselearningeveryday.util.DataSource;
import android.app.Activity;
import android.app.ActionBar.LayoutParams;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

public class ParallaxFragment extends Fragment {
	private DataSource datasource;
	public static final String VIEW_TYPE = "fragment_type";

	public ParallaxFragment() {
	} // empty constructor

	FragmentListener mCallback;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.parallax_layout, container, false);
		datasource = new DataSource(getActivity());
		datasource.open();
		LinearLayout linearLayout;

		// ----get the type of parallax view that we need: scenario, quiz,
		// phrase---
		int type = getArguments().getInt(VIEW_TYPE);
		int rightMenuID = 0;
		List<SubMenuClass> values = null;
		if (type == 1) {
			values = datasource.getAllTutorialTypes();
			rightMenuID = RightMenuFragment.SUB_MENU_BLANK;
		} else if (type == 2) {
			values = datasource.getAllScenarioTypes();
			rightMenuID = RightMenuFragment.SUB_MENU_SCENARIO;
		} else if (type == 3) {
			values = datasource.getAllPhrasesBook();
			rightMenuID = RightMenuFragment.SUB_MENU_PHRASES;
		} else {
			values = datasource.getAllQuizTypes();
			rightMenuID = RightMenuFragment.SUB_MENU_QUIZZES;
		}

		// ----inflate the view in file imagebg.xml, fill in the image and add
		// to the listview----

		int paddingTemp;
		
		Display display = getActivity().getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		
		paddingTemp=size.y/35;
		
		// get the parallax background
		linearLayout = (LinearLayout) rootView.findViewById(R.id.back_ground);
		
		linearLayout.setPadding(0, paddingTemp, 0, paddingTemp);
		
		//linearLayout.setPadding(0, paddingTemp, 0, paddingTemp);
		//linearLayout.setBackgroundResource(resid);
		for (int i = 0; i < values.size(); i++) {
			// get image view
			ImageView iV = (ImageView) inflater.inflate(R.layout.imagebg, linearLayout, false);
			
			ViewGroup.MarginLayoutParams p2 = (ViewGroup.MarginLayoutParams) iV.getLayoutParams();
			p2.setMargins(0, size.y/70, 0, paddingTemp);
			iV.requestLayout();
			// get the name of the image that need to put into image view
			String mDrawableName = values.get(i).getImg();
			// get id of the image from the name
			int resID = getResources().getIdentifier(mDrawableName, "drawable", getActivity().getPackageName());
			// in case image not found, will use a blank image
			if (resID == 0)
				resID = getResources().getIdentifier("empty", "drawable", getActivity().getPackageName());
			// set and add image into parallax background
			iV.setImageBitmap(
				    decodeSampledBitmapFromResource(getResources(), resID, 417, 196));
			//iV.setImageResource(resID);
			//iV.setPadding(0, 0, 0, 0);
			
			linearLayout.addView(iV);
			//iV.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		}

		// ----stretch the text view to the size of current screen----
		
		LinearLayout.LayoutParams Params1 = new LinearLayout.LayoutParams(width, LayoutParams.MATCH_PARENT);
		
		TextView t = (TextView) rootView.findViewById(R.id.text);
		t.setLayoutParams(Params1);
		paddingTemp=size.y/50;
		t.setPadding(paddingTemp, paddingTemp, paddingTemp, paddingTemp);
		// ----inflate content of each section into a content view and then put
		// into the parallax view----

		
       
        
		
		// get the parallax front
		linearLayout = (LinearLayout) rootView.findViewById(R.id.scroll_front);
		for (int i = 0; i < values.size(); i++) {
			// inflate content view
			LinearLayout v = (LinearLayout) inflater.inflate(R.layout.content, linearLayout, false);
			
			ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
			p.setMargins(0, (int)(size.y/5), 0, 0);
			v.requestLayout();
			
			int padding=size.y/57;
			
			v.setPadding(padding, padding , padding,padding);
			// get the text view in content and set text to it
			TextView tv = (TextView) v.findViewById(R.id.text_content);
			tv.setText(values.get(i).getDescriptions());
			tv.setTextSize(size.y/90);
			// get the button view and set text to it
			tv = (TextView) v.findViewById(R.id.title);
			tv.setText(values.get(i).getName());
			tv.setTextSize(size.y/70);
			
			Button btt = (Button) v.findViewById(R.id.button_content);
			//btt.setText(values.get(i).getName());
			// add content to the parallax front
			btt.setId(values.get(i).getId());
			btt.setOnClickListener(new PButtonListener());
			linearLayout.addView(v);
		}

		datasource.close();
		mCallback.showRightMenu(rightMenuID);
		return rootView;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mCallback = (FragmentListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement FragmentListener");
		}
	}

	public class PButtonListener implements Button.OnClickListener {

		@Override
		public void onClick(View v) {
			// communicate with main activity
			mCallback.showItem(getArguments().getInt(VIEW_TYPE), v.getId(), -1);
		}

	}
	
	public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
    // Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;
		
		if (height > reqHeight || width > reqWidth) {
		
		    final int halfHeight = height / 2;
		    final int halfWidth = width / 2;
		
		    // Calculate the largest inSampleSize value that is a power of 2 and keeps both
		    // height and width larger than the requested height and width.
		    while ((halfHeight / inSampleSize) > reqHeight
		            && (halfWidth / inSampleSize) > reqWidth) {
		        inSampleSize *= 2;
		    }
		}

	    return inSampleSize;
	}
	
	public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
	        int reqWidth, int reqHeight) {

	    // First decode with inJustDecodeBounds=true to check dimensions
	    final BitmapFactory.Options options = new BitmapFactory.Options();
	    options.inJustDecodeBounds = true;
	    BitmapFactory.decodeResource(res, resId, options);

	    // Calculate inSampleSize
	    options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

	    // Decode bitmap with inSampleSize set
	    options.inJustDecodeBounds = false;
	    return BitmapFactory.decodeResource(res, resId, options);
	}

}
