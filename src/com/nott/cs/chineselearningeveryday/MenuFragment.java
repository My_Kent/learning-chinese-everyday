package com.nott.cs.chineselearningeveryday;

/**
 * @author Vm Tung txv03u
 */

import com.nott.cs.chineselearningeveryday.util.MainMenuAdapter;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.AdapterView.OnItemClickListener;

public class MenuFragment extends Fragment {

	FragmentListener mCallback;

	static final String[] menuText = new String[] { "Tutorial", "Scenarios", "Phrase Book", "Words","Quizzes" };
	// literal for now
	GridView gridView; // grid view

	public MenuFragment() {
	} // empty constructor

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		

		View rootView = inflater.inflate(R.layout.main_menu_fragment, container, false);

		gridView = (GridView) rootView.findViewById(R.id.gridview);
		gridView.setAdapter(new MainMenuAdapter(getActivity(), menuText));
		gridView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				mCallback.onMenuItemSelected(position + 1);

			}
		});

		mCallback.showRightMenu(RightMenuFragment.MAIN_MENU);
		mCallback.lockRightDrawer(false);

		return rootView;

	}

	// ----initialise mCallback to be able to communicate with Main activity----
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mCallback = (FragmentListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement FragmentListener");
		}
	}

	// ----button listener and communicate with main activity ----
	public class MenuButtonListener implements Button.OnClickListener {
		int i;

		public MenuButtonListener(int i) {
			this.i = i;
		}

		@Override
		public void onClick(View v) {
			// communicate with main activity
			mCallback.onMenuItemSelected(i + 1);
		}

	}

}
